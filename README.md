# AtmelOS #

A project to implement a simple, scheduling operating system for Atmel processors.

### Setup ###

* TODO
* Version: 1.0

### Supported CPUs ###
* 328P

### Working features ###

* Concurrency between processes
* Timer interrupts for each process
* Keyboard interrupts for each process
* Process creation/destroying