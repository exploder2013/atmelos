/*
 * OS_Macro.asm
 *
 *  Created: 25.12.2017 21:44:56
 *   Author: Normunds
 */ 

#ifndef H_OS_MACRO
#define H_OS_MACRO

; ------------------------ MACROS -----------------------------

 .macro STACK_INIT
	ldi r16, low(RAMEND)
	out SPL, r16
	ldi r16, high(RAMEND)
	out SPH, r16
.endmacro

.macro GET_STACK_REG
	in		XH, SPH
	in		XL, SPL
.endmacro

.macro GET_SREG
	in		r24, SREG
.endmacro

.macro pushall
	; preserve all general purpose registers
    push    r0
    push    r1
    push    r2
    push    r3
    push    r4
    push    r5
    push    r6
    push    r7
    push    r8
    push    r9
    push    r10
    push    r11
    push    r12
    push    r13
    push    r14
    push    r15
    push    r16
    push    r17
    push    r18
    push    r19
    push    r20
    push    r21
    push    r22
    push    r23
    push    r24
    push    r25
    push    r26
    push    r27
    push    r28
    push    r29
    push    r30
    push    r31 
.endmacro

.macro popall
	; retrieve all general purpose registers
    pop    r31
    pop    r30
    pop    r29
    pop    r28
    pop    r27
    pop    r26
    pop    r25
    pop    r24
    pop    r23
    pop    r22
    pop    r21
    pop    r20
    pop    r19
    pop    r18
    pop    r17
    pop    r16
    pop    r15
    pop    r14
    pop    r13
    pop    r12
    pop    r11
    pop    r10
    pop    r9
    pop    r8
    pop    r7
    pop    r6
    pop    r5
    pop    r4
    pop    r3
    pop    r2
    pop    r1
    pop    r0 
.endmacro

; ------------------------ FUNCTIONS ---------------------------

; Unsigned 8bit divison
; r25 - number/reminder
; r24 - divider/divisor
div8u:
	push	r16
	clr		r16

	; TODO: Undefined result if divisor == 0

div8u_sub:
	inc		r16
	sub		r25, r24
	brcc	div8u_sub

	; Get reminder
	add		r25, r24

	; Fix divisor
	dec		r16

	; Move divisor in r24
	mov		r24, r16

div8u_ret:
	pop		r16
	ret

#endif
