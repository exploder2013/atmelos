/*
 * Time.asm
 *
 *  Created: 25.12.2017 22:24:08
 *   Author: Normunds
 */ 

#ifndef H_OS_TIME
#define H_OS_TIME

; Name:     delay_ms
; Purpose:  Delays for r16 * 1ms of time
; Entry:    r16 for the amount of ms it should wait for
; Exit:     
; Notes:	for example, if r16 was 5, it would delay for 5ms
delay_ms:
    call    delay_1ms     ; delay for 1 mS
    dec     r16			  ; update the delay counter
    brne    delay_ms    ; counter is not zero

; arrive here when delay counter is zero (total delay period is finished)
    ret

; ---------------------------------------------------------------------------
; Name:     delay_1ms
; Purpose:  provide a delay of 1 ms
; Entry:    no parameters
; Exit:     no parameters
; Notes:    chews up fclk/1000 clock cycles (including the 'call')

delay_1ms:
    push    YL                              ; [2] preserve registers
    push    YH                              ; [2]
    ldi     YL, low (((fclk/1000)-18)/4)    ; [1] delay counter
    ldi     YH, high(((fclk/1000)-18)/4)    ; [1]

delay_1ms_01:
    sbiw    YH:YL, 1                        ; [2] update the the delay counter
    brne    delay_1ms_01                     ; [2] delay counter is not zero

; arrive here when delay counter is zero
    pop     YH                              ; [2] restore registers
    pop     YL                              ; [2]
    ret                       

; Name:     delay_us
; Purpose:  provide a delay of r16 x 10 of microseconds
; Entry:    Delays for r16 * 10us of time 
; Exit:     no parameters
; Notes:    the 8-bit register provides for a delay of up to 2550 us (2.5ms)

delay_us:
	push	r17

delay_us_0:
	ldi		r17, 9

delay_us_1:
    call    delay_1us                       ; delay for 1 uS
	dec		r17
	brne	delay_us_1

    dec     r16								; decrement the delay counter
    brne    delay_us_0						; counter is not zero

; arrive here when delay counter is zero (total delay period is finished)
	pop		r17
    ret

; ---------------------------------------------------------------------------
; Name:     delay_1us
; Purpose:  provide a delay of 1 us
; Entry:    no parameters
; Exit:     no parameters
delay_1us:
    push    temp_r16                            ; [2] these instructions do nothing except consume clock cycles
    pop     temp_r16                            ; [2]
    push    temp_r16                            ; [2]
    pop     temp_r16                            ; [2]
    ret											; [4]

; Name:     delay_10ms
; Purpose:  Waits for specific amount of time before continuing.
; Entry:    r16 for the time in 40000Hz increments to wait for.
; Exit:     
; Notes:	for example, if r16 was 100, it would wait for 1600000 clock cycles
delay_10x_ms:
	push	r16
	push	ZH
	push	ZL

delay_outer:
	ldi ZH, HIGH( 40000 )
	ldi ZL, LOW( 40000 )

delay_inner:
	sbiw ZL, 1
	brne delay_inner

	dec  r16
	brne delay_outer

	pop		ZL
	pop		ZH
	pop		r16
	ret

#endif
