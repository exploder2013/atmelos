/*
 * Process.asm
 *
 *  Created: 25.12.2017 23:18:59
 *   Author: Normunds
 */ 

#ifndef H_OS_PROCESS
#define H_OS_PROCESS

GetRunningProcessCount:	
	lds		r25, process_count
	ret

SetRunningProcessCount:
	sts		process_count, r25
	ret

GetCurrentSREG:
	lds		r24, current_sreg
	ret

SetCurrentSREG:
	sts		current_sreg, r24
	ret

GetCurrentPID:	
	lds		r25, PID
	ret

SetCurrentPID:
	sts		PID, r25
	ret

GetProcessSPS:
	ldi		ZH, high(process_sps)
	ldi		ZL, low(process_sps)
	ret

GetProcessSREGS:
	ldi		ZH, high(process_sregs)
	ldi		ZL, low(process_sregs)
	ret

GetInitialSP:
	lds		ZH, initial_sp
	lds		ZL, initial_sp + 1
	ret

; Checks whether address at ZH&ZL is a valid function
; r24 - TRUE if valid, FALSE if invalid
IsValidFunction:
	push	r16
	clr		r24

; Check high-bits
	ld		r16, Z+
	tst		r16
	breq	IsValidFunction_invalid

; Check low-bits
	ld		r16, Z

	; Reset Z pointer
	sbiw	ZH:ZL, 1

	tst		r16
	breq	IsValidFunction_invalid

	; If valid, increment r24, to make it TRUE
	inc		r24

IsValidFunction_invalid:
	pop		r16
	ret

; r25 - procID from which the SREG is needed
GetProcessSREG:
	push	r16

	; used for adc
	clr		r16

	; Calculate SREGS offset ( 1byte )
	call	GetProcessSREGS

	add		ZL, r25
	adc		ZH, r16

	pop		r16
	ret

; r25 - procID from which the SP is needed
GetProcessSPActualOffset:
	push	r25

	; Get initial stack position in ZH&ZL
	call	GetInitialSP

	; Return if r25 == 0
	tst		r25
	breq	GetProcessSP_loop_end
	
GetProcessSP_loop:
	subi	ZL, DEFAULT_PROCESS_SP_SIZE
	sbci	ZH, 0

	dec		r25
	brne	GetProcessSP_loop

GetProcessSP_loop_end:
	pop		r25
	ret

; ZH&ZL - Process context start
GetProcessSPSubContextAndReturnOffset:
	; Add CONTEXT_SIZE to Z, because we are pushing whole
	; context so, we need to be able to restore it when doing popall
	sbiw	ZH:ZL, CONTEXT_SIZE + 2 ; +2 because return address is 2 bytes big
	ret

;  r25 - procID that specifies to which process SREG will be saved to
SaveProcessSREG:
	push	r24
	push	ZH
	push	ZL

	call	GetCurrentSREG
	call	GetProcessSREG

	st		Z, r24

	pop		ZL
	pop		ZH
	pop		r24
	ret

;  r25 - procID that specifies to which process SP to get
GetProcessSP:
	push	r16
	push	r25

	; used for adc
	clr		r16

	; r25 * 2
	lsl		r25

	; Calculate SP offset ( 2bytes )
	call	GetProcessSPS

	add		ZL, r25
	adc		ZH, r16

	pop		r25
	pop		r16

	ret

;  r25 - procID that specifies to which process SP will be saved to
;  XH & XL - SP value to save
SaveProcessSP:
	push	r25

	push	ZH
	push	ZL

	call	GetProcessSP

	st		Z+, XH
	st		Z,  XL

	pop		ZL
	pop		ZH

	pop		r25

	ret

GetProcessKeyboardInts:
	ldi		ZH, high(process_keyboard_ints)
	ldi		ZL, low(process_keyboard_ints)
	ret

; r25 - procID for which to get keyboard int
GetProcessKeyboardInt:
	push	r25
	push	r16

	clr		r16
	call	GetProcessKeyboardInts

	; Get offset at which the keyboard int is located (2 bytes)
	lsl		r25

	; Add the offset
	add		ZL, r25
	adc		ZH, r16

	pop		r16
	pop		r25
	ret

GetProcessPIDs:
	ldi		ZH, high(process_pids)
	ldi		ZL, low(process_pids)
	ret

; r25 - PID to look for
FindPID:
	push	r25
	push	r16
	push	ZH
	push	ZL

	ldi		r24, PID_BUFFER_SIZE
	inc		r24

	call	GetProcessPIDs

	; Iterate through the array
FindPID_loop_0:
	dec		r24
	breq	FindPID_not_found	; We've reached the end of the array

	ld		r16, Z+
	cp		r16, r25
	brne	FindPID_loop_0

FindPID_found:
	; Make it relative to beginning (0th index - 1st element)
	subi	r24, PID_BUFFER_SIZE
	neg		r24
	rjmp	FindPID_ret

FindPID_not_found:
	ldi		r24, -1

FindPID_ret:
	pop		ZL
	pop		ZH
	pop		r16
	pop		r25

	ret

GetNextPID:
	push	r24
	push	r16

	push	ZH
	push	ZL

	; Try to find the current PID and then return the next one
	call	GetCurrentPID
	call	GetProcessPIDs

	call	FindPID
	cpi		r24, -1 ; PID not found
	breq	GetNextPID_not_found ; This should never happen!

	; Add 1 to offset because we need the next element
	clr		r16
	inc		r24

	add		ZL, r24
	adc		ZH, r16

GetNextPID_found:
	; Look for the next PID, that is valid ( pid != -1 )
	
	; If we're at the end, just return SYSTEM PID, so SYSTEM process can run
	cpi		r24, PID_BUFFER_SIZE - 1
	brge	GetNextPID_not_found

	inc		r24

	ld		r16, Z+
	cpi		r16, INVALID_PID
	breq	GetNextPID_found

	; If we are here, then a valid next PID was found.
	mov		r25, r16
	rjmp	GetNextPID_ret

GetNextPID_not_found:
	ldi		r25, SYSTEM_INDEX	; Return SYSTEM process

GetNextPID_ret:
	pop		ZL
	pop		ZH

	pop		r16
	pop		r24
	ret

; Try to find free PID, by brute force
GetFreePID:
	push	r24

	call	GetCurrentPID
	rjmp	GetFreePID_loop

GetFreePID_reset:
	clr		r25

GetFreePID_loop:
	inc		r25
	cpi		r25, MAX_PROCESS_COUNT + 1 ; We're limited by max process count for our PIDs
	brge	GetFreePID_reset

	call	FindPID
	cpi		r24, -1
	brne	GetFreePID_loop

GetFreePID_found:

	pop		r24
	ret

; r25 - PID to add
AddProcessPID:
	push	r24

	push	ZH
	push	ZL

	call	GetProcessPIDs

	push	r25
	; Use function to find empty spot in array
	ldi		r25, -1
	call	FindPID

	clr		r25

	add		ZL, r24
	adc		ZH, r25

	; Restore passed argument
	pop		r25
	st		Z, r25

	pop		ZL
	pop		ZH

	pop		r24
	ret

; r25 - PID to remove
RemoveProcessPID:
	push	r24
	push	r25

	push	ZH
	push	ZL

	call	FindPID
	cpi		r24, -1
	breq	RemoveProcessPID_end ; Couldn't find the PID

	call	GetProcessPIDs
	clr		r25

	add		ZL, r24
	adc		ZH, r25

	; Replace PID with INVALID_PID entry
	; Which will technically remove the process from running process list
	ldi		r25, INVALID_PID
	st		Z, r25

RemoveProcessPID_end:
	pop		ZL
	pop		ZH

	pop		r25
	pop		r24
	ret

GetProcessTimerInts:
	ldi		ZH, high(process_timer_ints)
	ldi		ZL, low(process_timer_ints)
	ret

; r25 - procID for which process to get the interrupt
GetProcessTimerInt:
	push	r25
	push	r16

	; Used for adc
	clr		r16

	call	GetProcessTimerInts

	; Get offset at which the timer int is located (2 bytes)
	lsl		r25

	; Add the offset
	add		ZL, r25
	adc		ZH, r16

	pop		r16
	pop		r25
	ret

GetProcessTimerOverflows:
	ldi		ZH, high(process_timer_overflows)
	ldi		ZL, low(process_timer_overflows)
	ret

; r25 - procID for which process to get the timer overflow
GetProcessTimerOverflow:
	push	r16

	; Used for adc
	clr		r16

	call	GetProcessTimerOverflows

	; Add the offset
	add		ZL, r25
	adc		ZH, r16

	pop		r16
	ret

#endif
