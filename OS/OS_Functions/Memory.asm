/*
 * Memory.asm
 *
 *  Created: 25.12.2017 23:30:15
 *   Author: Normunds
 */ 

#ifndef H_OS_MEMORY
#define H_OS_MEMORY

; ZH & ZL	- dst
; XH & XL	- src
; R16		- size
memcpy:
	push	ZH
	push	ZL

	push	XH
	push	XL

	push	r17

	; Test if r16 == 0
	tst		r16
	breq	memcpy_ret

memcpy_loop:
	ld		r17, X+
	st		Z+, r17

	dec		r16
	brne	memcpy_loop

memcpy_ret:
	pop		r17

	pop		XL
	pop		XH

	pop		ZL
	pop		ZH

	ret

; ZH & ZL	- dst
; R16		- size
memclr:
	push	ZH
	push	ZL

	push	r17
	clr		r17

	; Test if r16 == 0
	tst		r16
	breq	memclr_ret

memclr_loop:
	st		Z+, r17
	dec		r16
	brne	memclr_loop

memclr_ret:
	pop		r17

	pop		ZL
	pop		ZH

	ret

; ZH & ZL	- dst
; R17		- byte to set
; R16		- size
memset:
	push	ZH
	push	ZL

	; Test if r16 == 0
	tst		r16
	breq	memset_ret

memset_loop:
	st		Z+, r17
	dec		r16
	brne	memset_loop

memset_ret:
	pop		ZL
	pop		ZH

	ret

#endif
