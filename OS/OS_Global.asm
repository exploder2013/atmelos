/*
 * Global.asm
 *
 *  Created: 10.12.2017 18:26:50
 *   Author: Normunds
 */ 

#ifndef H_OS_GLOBAL
#define H_OS_GLOBAL

; For memclr/memcpy/memset
.include "OS_Functions\Memory.asm"

.dseg

	; Stack
	initial_sp:					.byte 2 ; Holds the initial SP position, so we can use this as the base and
										; add offset to it, to set SP for different processes.

	.equ DEFAULT_PROCESS_SP_SIZE = 128  ; Holds how many bytes each process gets allocated for its stack memory
										; Default 128 bytes

	.equ SPRAM_SIZE				 = 2048 ; ( 2KB ) Holds how much RAM this device has
	.equ CONTEXT_SIZE			 = 32   ; How big the context is for a process (32x registers)

	; SREG
	current_sreg:				.byte 1 ; Holds the current SREG register

	; Process
	.equ SYSTEM_INDEX			= 0
	.equ MAX_PROCESS_COUNT		= SPRAM_SIZE / DEFAULT_PROCESS_SP_SIZE

	.equ INVALID_PID			= 0xFF
	PID:						.byte 1 ; Holds the current process ID. ( 0 == system )

	.equ PID_BUFFER_SIZE		= MAX_PROCESS_COUNT ; ( 2048 / 128 (context size) == 16 )
	process_pids:				.byte PID_BUFFER_SIZE ; Holds all process running PIDs

	process_count:				.byte 1 ; Holds the amount of processes running.
	

	; Allocate memory for struct to save process status registers in
	.equ SREG_BUFFER_SIZE		= MAX_PROCESS_COUNT ; ( 2048 / 128 (context size) == 16 )
	process_sregs:				.byte SREG_BUFFER_SIZE

	; Allocate memory for struct to save process SP register in
	.equ SP_BUFFER_SIZE			= MAX_PROCESS_COUNT * 2 ; ( 2048 / 128 * 2 == 32 )
	process_sps:				.byte SP_BUFFER_SIZE

	; Create priorities for processes
	; These represent after how many clock ticks
	; a context switch is going to happen
	.equ	HIGH_PRIORITY		= 3
	.equ	MEDIUM_PRIORITY		= 2
	.equ	LOW_PRIORITY		= 1

	; Keyboard
	.equ	KEYBOARD_DEBOUNCE_DELAY	= 10

	; Create better names for buttons
	.equ BTN_NONE		= -1
	.equ BTN_SELECT		= 0
	.equ BTN_LEFT		= 1
	.equ BTN_RIGHT		= 4
	.equ BTN_UP			= 3
	.equ BTN_DOWN		= 2

	btn_state:			.byte 1 ; Contains the current button state from last input

	; Allocate memory for struct to save process keyboard interrupt in buffer
	.equ KB_BUFFER_SIZE			= MAX_PROCESS_COUNT * 2 ; ( 2048 / 128 * 2 == 32 )
	process_keyboard_ints:		.byte KB_BUFFER_SIZE

	; Timer
	overflow_count:			.byte 1 ; Contains how many times timer overflowed
	max_overflow:			.byte 1 ; Used for reseting overflow back to 0, to increase performance

	; Allocate memory for struct to save timer keyboard interrupt in buffer
	.equ TIMER_BUFFER_SIZE		= MAX_PROCESS_COUNT * 2 ; ( 2048 / 128 * 2 == 32 )
	process_timer_ints:		.byte TIMER_BUFFER_SIZE

	; Allocate memory for struct to save timer overflow times at which to launch the interrupts
	.equ TIMER_OVERFLOW_SIZE	= MAX_PROCESS_COUNT ; ( 2048 / 128 == 16 )
	process_timer_overflows:.byte TIMER_OVERFLOW_SIZE

	; Indicates that the process doesn't use a timer (-1)
	.equ PROCESS_TIMER_NOT_ACTIVE	= 0xFF

	; Focus
	active_process_pid:		.byte 1 ; Holds the currently active display PID.
									; It is used to check whether we can draw on screen or not

.cseg

; Name:     global_init
; Purpose:  Initilises the global variables to defined values
; Exit:     
; Notes:   
OSGlobalInit:
	push	r16
	push	ZH
	push	ZL

	; Store SRAM end
	ldi		r16, high(RAMEND)
	sts		initial_sp, r16

	ldi		r16, low(RAMEND)
	sts		initial_sp + 1, r16

	; Rest overflow_count to 0
	clr		r16
	sts		overflow_count, r16

	; Set current PID to SYSTEM (0)
	sts		PID, r16

	; Set current running process count to 0
	sts		process_count, r16

	; Set process timer/keyboard int routines to null.
	ldi		ZH, high(process_keyboard_ints)
	ldi		ZL, low(process_keyboard_ints)

	ldi		r16, KB_BUFFER_SIZE
	call	memclr

	ldi		ZH, high(process_timer_ints)
	ldi		ZL, low(process_timer_ints)

	ldi		r16, TIMER_BUFFER_SIZE
	call	memclr

	; Set all process timer overflow times to -1 (not active)
	ldi		ZH, high(process_timer_overflows)
	ldi		ZL, low(process_timer_overflows)

	ldi		r16, TIMER_OVERFLOW_SIZE
	ldi		r17, PROCESS_TIMER_NOT_ACTIVE
	call	memset

	; Set all process PIDs to invalid, except the first one (SYSTEM)
	ldi		ZH, high(process_pids)
	ldi		ZL, low(process_pids)

	ldi		r16, PID_BUFFER_SIZE
	ldi		r17, INVALID_PID
	call	memset

	; Set first entry as SYSTEM
	ldi		r16, SYSTEM_INDEX
	st		Z+, r16

	; Set active display as OS for startup
	clr		r16
	sts		active_process_pid, r16

	; Set current max overflow to 1
	; to avoid constant resets
	ldi		r16, 0xFF
	sts		max_overflow, r16


	pop		ZL
	pop		ZH
	pop		r16
	ret

#endif
