/*
 * Timer.asm
 *
 *  Created: 25.12.2017 22:07:24
 *   Author: Normunds
 */ 

#ifndef H_OS_TIMER
#define H_OS_TIMER

.include "OS_Functions\Process.asm"
.include "OS_Functions\Memory.asm"

; switches the registers of one process to another
; r25 - procID of process to swap context to
OS_SwitchContext:
	
	pushall

	mov		r23, r25
	call	GetCurrentPID
	cp		r23, r25

	; We're trying to swap context with our own process, exit!
	breq	OS_SwitchContext_end

	; Save SP from current process
	GET_STACK_REG
	call	SaveProcessSP

	; Get other process SP
	mov		r25, r23
	call	GetProcessSP

	; Set new SP
	ld		r16, Z+
	out		SPH, r16
	ld		r16, Z
	out		SPL, r16

OS_SwitchContext_end:
	popall

	ret

OS_HandleTimerInput:

	; Save registers from current process
	pushall
	
	call	GetCurrentPID
	; Save SREG from current process
	call	SaveProcessSREG

	; Save SP from current process
	GET_STACK_REG
	call	SaveProcessSP
	
	; Get next process SP
	call	GetNextPID
	call	GetProcessSP

	; Set new SP
	ld		r16, Z+
	out		SPH, r16
	ld		r16, Z
	out		SPL, r16

	; Set SREG for the next process
	call	GetProcessSREG
	ld		r16, Z

	out		SREG, r16

	; Set new PID for next process (saved in r25 from GetNextPID call)
	call	SetCurrentPID

	; Restore registers from next process
	; SP is now pointing to the next process ending after pushall
	popall


os_handle_timer_ret:
	reti

; Handles the interrupts for all processes that have registered
; their timer interrupts so they can use the Timer
OS_HandleTimerInterrupts:
	push	r23
	push	r24
	push	r25

	push	ZH
	push	ZL
	push	XH
	push	XL

	; Check if overflow count == 0, terminate if so
	; because we can't divide by 0
	lds		r25, overflow_count
	tst		r25
	breq	OS_HandleTimerInterrupts_ret

	; We need to iterate through all processes and
	; check if they have a timer interrupt set

	; We save the current PID
	call	GetCurrentPID
	mov		r23, r25

	rjmp	OS_HandleTimerInterrupts_CheckOverflow

OS_HandleTimerInterrupts_TestNext:
	call	GetNextPID
	call	SetCurrentPID

	; Check if we have walked full circle in process list
	; if so, return (firstPID == currentPID)
	cp		r23, r25
	breq	OS_HandleTimerInterrupts_ret

OS_HandleTimerInterrupts_CheckOverflow:
	call	GetProcessTimerOverflow

	; Check if its valid timer
	ld		r24, Z
	cpi		r24, PROCESS_TIMER_NOT_ACTIVE
	breq	OS_HandleTimerInterrupts_TestNext

OS_HandleTimerInterrupts_Call:
	; Process has registered its timer, check if its time to call it

	; r25 - overflow count, r24 - process overflow fire time
	lds		r25, overflow_count
	call	div8u

	; Check if reminder == 0, then call the timer subroutine
	tst		r25
	brne	OS_HandleTimerInterrupts_TestNext

	; Get subroutine of current PID
	call	GetCurrentPID
	call	GetProcessTimerInt

	; Dereference
	mov		XH, ZH
	mov		XL, ZL

	ld		ZH, X+
	ld		ZL, X

	; Call it
	icall

	rjmp	OS_HandleTimerInterrupts_TestNext

OS_HandleTimerInterrupts_ret:
	pop		XL
	pop		XH
	pop		ZL
	pop		ZH

	pop		r25
	pop		r24
	pop		r23
	ret

#endif
