/*
 * Keyboard.asm
 *
 *  Created: 25.12.2017 22:07:33
 *   Author: Normunds
 */ 

#ifndef H_OS_KEYBOARD
#define H_OS_KEYBOARD

.include "OS_Functions\Process.asm"
.include "OS_Functions\Time.asm"

OS_HandleKeyboardInput:
	push	r25
	push	r24
	push	r16

	push	ZH
	push	ZL
	push	XH
	push	XL
	
	; We only want to process keyboard interrupts for the current process,
	; as that would cause user to press unintended keys for the other processes
	call	GetActiveProcessPID

	; Save it, becuase if we do a process switch, we will need to add delay
	; because we don't want the next process to have our previous keys recoreded
	mov		r24, r25

	; Get keyboard subroutine offset
	call	GetProcessKeyboardInt

	; Copy so we can read from it later and 
	; place it inside Z register for icall
	mov		XH, ZH
	mov		XL, ZL
	; TODO: Will not work if highbit will be 0
	; Add two checks for both high/low bits

	; Check if the process uses keyboard int
	; null otherwise (high bit)
	ld		r16, X
	tst		r16
	breq	OS_HandleKeyboardInput_ret

	; Add delay to not have next process record the new input for a bit
	; This could be solved by putting a delay in functions like
	; MinimiseProcess/etc, but this also solves the problem that
	; too many inputs get recorded for ordinary programs as-well.
	; The only problem is that this limits the speed of key presses to 150ms
	; So if a process needed keys for fast input, then this needs to be changed.
	; TODO??
	;ldi	r16, 15
	;call	delay_10x_ms 

OS_HandleKeyboardInput_call:
	; Swap the registers/stack so we're executing the subroutine in process address space
	; TODO: Implement completly
	;call	OS_SwitchContext
	
	; Load the subroutines address
	ld		ZH, X+
	ld		ZL, X

	; Call the keyboard subroutine for the current process
	icall

	;call	GetCurrentPID
	; Swap the registers/stack back
	; TODO: Implement completly
	;call	OS_SwitchContext

OS_HandleKeyboardInput_ret:
	pop		XL
	pop		XH
	pop		ZL
	pop		ZH

	pop		r16
	pop		r24
	pop		r25
	ret

#endif
