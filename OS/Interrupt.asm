/*
 * Input.asm
 *
 *  Created: 07.12.2017 00:51:07
 *   Author: Normunds
 */ 


#ifndef H_OS_INT
#define H_OS_INT

.nolist
.include "m328Pdef.inc"
.list

.def r23btn_state = r23 ; Button state on interrupt is held here

.include "OS_Global.asm"

.include "Keyboard\Keyboard.asm"
.include "Timer\Timer.asm"

;.dseg
;	button:			.byte 1 ; Contains the currently pressed button

.cseg

; Name:     interrupt_init 
; Purpose:  Enables the input interrupt, as well does initilisation
; Entry:    uses no arguments
; Exit:     input interupt should be enabled
; Notes:  
OSInterruptInit:
	push	r16
	clr		r16
	
	call	SetupKeyboardInterrupt
	call	SetupTimerInterrupt
	
	; Enable interrupts
	sei

	pop		r16
	ret


; Name:     register_keyboard_int 
; Purpose:  Sets up and registers the keyboard interrupt
; Entry:    Uses no arguments
; Exit:     Saves the button pressed in data
; Notes:  
SetupKeyboardInterrupt:
	push	r16

	ldi		r16, -1 ; Set the initial button state to -1 (no button pressed)
	sts		btn_state, r16

	lds		r16, PRR
	andi	r16, 0xFE
	sts		PRR, r16

	ldi		r16, 0b01100000
	sts		ADMUX, r16

	lds		r16, DIDR0
	ori		r16, ( 1 << ADC0D )
	sts		DIDR0, r16

	ldi		r16, ( 1 << ISC01 ) | ( 1 << ISC00 )
	sts		EICRA, r16
	
	eor		r16, r16 ; Set r16 = 0
	out		EIMSK, r16

	ldi		r16, 0b11101111
	sts		ADCSRA, r16

	eor		r16, r16
	sts		ADCSRB, r16

	ldi		r16, 0xFE
	out		DDRC, r16
	clr		r16
	out		PortC, r16

	pop		r16
	ret


; Name:     register_watchdog_int 
; Purpose:  Sets up and registers the watchdog (timer) interrupt
; Entry:    Uses no arguments
; Exit:     Saves the button pressed in data
; Notes:  
SetupTimerInterrupt:
	push	r16

	ldi		r16, 0b00000101		;Sets Clock Selector Bits CS00, CS01, CS02 to 101
	out		TCCR0B, r16

	ldi		r16, 0b00000001
	sts		TIMSK0, r16

	clr		r16
	out		TCNT0, r16

	;sbi		DDRB, 2
	
	pop		r16
	ret

; Name:     keyboard_vector 
; Purpose:  Handles the input interrupts and saves the button press
; Entry:    Uses no arguments
; Exit:     Saves the button pressed in data
; Notes:  
KeyboardVector:
	push	r24

	; Save, the SREG as this will cause invalid branching in the process
	GET_SREG
	call	SetCurrentSREG

	push	r23btn_state
	lds		r23btn_state, ADCH

inp_vec_0:
	;clc
	cpi		r23btn_state, 240
	brlo	inp_vec_1
	ldi		r23btn_state, BTN_NONE
	sts		btn_state, r23btn_state
	rjmp	input_return_1

inp_vec_1:
	cpi		r23btn_state, 140
	brlo	inp_vec_2
	ldi		r23btn_state, BTN_SELECT
	rjmp	input_return_0

inp_vec_2:
	cpi		r23btn_state, 90
	brlo	inp_vec_3
	ldi		r23btn_state, BTN_LEFT
	rjmp	input_return_0

inp_vec_3:
	cpi		r23btn_state, 55
	brlo	inp_vec_4
	ldi		r23btn_state, BTN_DOWN
	rjmp	input_return_0

inp_vec_4:
	cpi		r23btn_state, 20
	brlo	inp_vec_5
	ldi		r23btn_state, BTN_UP
	rjmp	input_return_0

inp_vec_5:
	ldi		r23btn_state, BTN_RIGHT

input_return_0:
	sts		btn_state, r23btn_state	; Save the state on variable
	call	OS_HandleKeyboardInput	

input_return_1:

	; Restore the SREG, so our branches are correct
	call	GetCurrentSREG
	out		SREG, r24

	pop		r23btn_state
	pop		r24
	reti


TimerVector:
	push	r16
	push	r24

	; Save, just in case we're coming from another process
	GET_SREG
	call	SetCurrentSREG
	pop		r24

	; Reset timer if its reached the max value to save time on division
	push	r17

	lds		r17, max_overflow
	lds		r16, overflow_count

	cp		r16, r17
	pop		r17

	brne	TimerVector_continue

TimerVector_reset_timer:
	clr		r16
	;sts		overflow_count, r16 We store it below

TimerVector_continue:

	; Increment timer
	;lds		r16, overflow_count	 R16 is loaded already
	inc		r16
	sts		overflow_count, r16

	; Restore old state of r16
	pop		r16

	; Process timer interrupts for all processes
	call	OS_HandleTimerInterrupts

	push	r25
	; Check if there are any other processes running.
	; If not, just return, as we don't need to do context switch
	call	GetRunningProcessCount
	tst		r25
	
	pop		r25
	breq	timer_vector_end

	; Check if its time to switch 
	; depending on priority of the process
	; TODO

	; Process interrupt
	jmp		OS_HandleTimerInput

timer_vector_end:
	reti

#endif
