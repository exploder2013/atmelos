;
; AtmelOS.asm
;
; Created: 25.12.2017 21:35:03
; Author : Normunds
;


; Specs: 
;	2KB SRAM
;	1KB EEPROM
;	32KB PROGRAM MEMORY

.nolist
.include "m328Pdef.inc"
.list

.cseg

; Register interrupts
.org 0x0		; Reset
	jmp OSMain

.org 0x2A		; Keyboard
	jmp KeyboardInterrupt

.org 0x20		; Timer overflow
	jmp TimerInterrupt
	
.org INT_VECTORS_SIZE

; Includes
.include "OS_Global.asm"
.include "OS_Utils.asm"
.include "LCD\LCD-lib.asm"
.include "Interrupt.asm"
.include "Menu\OS_Menu.asm"
.include "OS_Functions\Time.asm"
.include "User\ProcessUser.asm"
.include "User\Window.asm"

.include "..\Programs\testprog.asm"

OSMain:
	STACK_INIT

    call	OSGlobalInit
	call	OSInterruptInit
	call	OSMenuInit

	; Register OS Keyboard interrupt
	ldi		XH, high(OSKeyboardInterrupt)
	ldi		XL,  low(OSKeyboardInterrupt)
	call	RegisterKeyboardInt


	; Register OS Timer interrupt with delay of KEYBOARD_DEBOUNCE_DELAY overflows
	ldi		XH, high(OSTimerInterrupt)
	ldi		XL, low(OSTimerInterrupt)
	ldi		r25, KEYBOARD_DEBOUNCE_DELAY
	call	RegisterTimerInt

	call	lcd_init_4d

	; Draw main OS menu
os_main_loop:	
	call	OSMenuDisplay

	; As the menu refreshes every time a button is pressed, 
	; we can redraw it every 200ms, as it is not necessary at all
	; A halt with interrupts enabled would be the best solution
	ldi		r16, 20
	call	delay_10x_ms
    rjmp	os_main_loop

	; Should never get here
	BREAK

; Is being used on the main menu, to be able to launch/switch bwetween processes
OSKeyboardInterrupt:
	call	OSMenuKeyboardHandler
	ret

; Is being used for keyboard input to debounce it and make it stable
; Runs every 10 overflows ( 10 * 16.32ms = 163.2ms )
OSTimerInterrupt:
	call	DebounceInput
	ret

; ---------------- INTERRUPTS --------------------

KeyboardInterrupt:
	jmp		KeyboardVector


;CPU Frequency: 16MHz
;Prescaler: 1024
;Frequency: 15625Hz
;Time per tick: 0,064ms

;Time per overflow: 16,32ms
	; Overflows to 1s -> 61.27 (0.27 -> 4,4ms) (clock)
	; Overflows to 100ms -> 6.127 (0.127 -> 2,07ms) (chrono)
TimerInterrupt:
	jmp		TimerVector



