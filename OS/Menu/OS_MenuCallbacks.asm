/*
 * MenuCallbacks.asm
 *
 *  Created: 10.12.2017 23:28:21
 *   Author: Normunds
 */ 

#ifndef H_OS_MENU_CALLBACKS
#define H_OS_MENU_CALLBACKS

; Include all programs that appear on menu here
.include "..\Programs\Chrono\Chrono.asm"
.include "..\Programs\Clock\Clock_Main.asm"

.dseg
	menu_process_pids:	.byte MAX_PROCESS_COUNT

.cseg

.macro GetMenuPIDs
	ldi		ZH, high(menu_process_pids)
	ldi		ZL, low (menu_process_pids)
.endmacro

os_callback_menu1:
	push	ZH
	push	ZL
	push	XH
	push	XL

	push	r25

	; Check if process was ran previously
	GetMenuPIDs
	ld		r25, Z
	cpi		r25, INVALID_PID
	breq	callback_create_1

	; Check if it has terminated, if so, restart
	call	IsProcessRunning
	tst		r24
	brne	callback_focus_1

	; Create the process
callback_create_1:
	ldi		XH, high(ClockMain)
	ldi		XL, low(ClockMain)

	call	CreateProcess
	st		Z, r25

	; Set processes display as main focus
callback_focus_1:
	call	MaximiseProcess

callback_ret_1:
	pop		r25

	pop		XL
	pop		XH
	pop		ZL
	pop		ZH
	ret

os_callback_menu2:
	push	ZH
	push	ZL
	push	XH
	push	XL

	push	r25

	; Check if process was ran previously (2nd process)
	GetMenuPIDs
	adiw	ZH:ZL, 1

	ld		r25, Z
	cpi		r25, INVALID_PID
	breq	callback_create_2

	; Check if it has terminated, if so, restart
	call	IsProcessRunning
	tst		r24
	brne	callback_focus_2

	; Create the process
callback_create_2:
	ldi		XH, high(ChronoMain)
	ldi		XL, low(ChronoMain)

	call	CreateProcess
	st		Z, r25

	; Set processes display as main focus
callback_focus_2:
	call	MaximiseProcess

callback_ret_2:
	pop		r25

	pop		XL
	pop		XH
	pop		ZL
	pop		ZH
	ret

os_callback_menu3:
	push	r25
	push	r23
	push	r16

	; Clear the screen
	ldi		r16, lcd_Clear
	call	lcd_write_instruction_4d

	; We save the current PID
	call	GetCurrentPID
	mov		r23, r25

	rjmp	callback_menu3_print

callback_menu3_get_next:
	call	GetNextPID
	call	SetCurrentPID

	cp		r23, r25
	breq	callback_menu3_exit

callback_menu3_print:
	subi	r25, -'0'

	mov		r16, r25
	call	lcd_write_character_4d

	ldi		r16, 2
	call	delay_10x_ms

	ldi		r16, ','
	call	lcd_write_character_4d

	ldi		r16, 2
	call	delay_10x_ms

	rjmp	callback_menu3_get_next

callback_menu3_exit:
	
	; Display the running process list for 1sec
	ldi		r16, 100
	call	delay_10x_ms

	pop		r16
	pop		r23
	pop		r25
	ret

#endif
