/*
 * Menu.asm
 *
 *  Created: 10.12.2017 21:36:42
 *   Author: Normunds
 */ 

#ifndef H_OS_MENU
#define H_OS_MENU

.include "Menu\OS_MenuCallbacks.asm"
.include "OS_Functions\Time.asm"
.include "OS_Functions\Memory.asm"

.dseg
	
	os_current_index:	.byte 1
	os_max_index:		.byte 1


.equ os_menu_entry_count	= 2

.cseg

os_menu_entries:
	os_menu_entry1:		.db "Clock", 0
	os_menu_entry2:		.db "Chrono", 0, 0
	os_menu_entry3:		.db "Running proc.", 0

; Jumptable of menu entires (3 bytes)
os_menu_entries_jumptable:
	ldi		ZH, high(os_menu_entry1)
	ldi		ZL, low (os_menu_entry1)
	ret
	ldi		ZH, high(os_menu_entry2)
	ldi		ZL, low (os_menu_entry2)
	ret
	ldi		ZH, high(os_menu_entry3)
	ldi		ZL, low (os_menu_entry3)
	ret

; Jumptable of menu callbacks (1 byte)
os_menu_callbacks_jumptable:
	jmp		os_callback_menu1
	jmp		os_callback_menu2
	jmp		os_callback_menu3



; Name:     OSMenuInit
; Purpose:  Initilises all menu variables.
; Exit:     no parameters
; Notes:   
OSMenuInit:
	push	r16
	push	ZH
	push	ZL

	clr		r16
	sts		os_current_index, r16

	ldi		r16, os_menu_entry_count
	sts		os_max_index, r16

	ldi		ZH, high(menu_process_pids)
	ldi		ZL, low(menu_process_pids)

	ldi		r16, MAX_PROCESS_COUNT
	ldi		r17, INVALID_PID
	call	memset

	pop		ZL
	pop		ZH
	pop		r16
	ret

; Name:     menu_display
; Purpose:  Responsible for displaying menu entries on LCD
; Exit:     no parameters
; Notes:   
OSMenuIncrement:
	push	r16
	push	r17

	lds		r16, os_current_index
	lds		r17, os_max_index

	; Check if we have reached the last entry. If so, reset index.
	cp		r16, r17
	breq	os_menu_increment_reset

os_menu_increment_add:
	inc		r16
	rjmp	os_menu_increment_ret

os_menu_increment_reset:
	clr		r16

os_menu_increment_ret:
	sts		os_current_index, r16

	pop		r17
	pop		r16
	ret

; Name:     menu_display
; Purpose:  Responsible for displaying menu entries on LCD
; Exit:     no parameters
; Notes:   
OSMenuDecrement:
	push	r16
	push	r17

	lds		r16, os_current_index
	lds		r17, os_max_index

	; Check if we have reached the first entry. If so, set index to last entry.
	cpi		r16, 0
	breq	os_menu_decrement_set_last

os_menu_decrement_dec:
	dec		r16
	rjmp	os_menu_decrement_ret

os_menu_decrement_set_last:
	mov		r16, r17

os_menu_decrement_ret:
	sts		os_current_index, r16

	pop		r17
	pop		r16
	ret

; Name:     OSMenuDisplay
; Purpose:  Responsible for displaying menu entries on LCD
; Exit:     no parameters
; Notes:   
OSMenuDisplay:
	push	r16
	push	r17
	push	r25

	push	ZH
	push	ZL

	; Only draw when display is active
	call	IsMainProcess
	tst		r25
	breq	os_menu_display_ret

	ldi		r16, lcd_Clear
	call	lcd_write_instruction_4d

	lds		r16, os_current_index
	lds		r17, os_max_index

	; Decide whether there is two or one entry to display ( if at the end of menu )
	sub		r17, r16
	tst		r17

	; If r17 == 0, then display one
	brne	os_display_double

os_display_single:
	call	OSMenuGetEntryByIndex

	ldi		r16, lcd_LineOne
	call	lcd_write_string_4d_code

	ldi		r16, '<'
	call	lcd_write_character_4d

	rjmp	os_menu_display_ret
os_display_double:
	push	r16
	call	OSMenuGetEntryByIndex

	ldi		r16, lcd_LineOne
	call	lcd_write_string_4d_code

	ldi		r16, '<'
	call	lcd_write_character_4d

	; Need this because otherwise, the other line gets written on the same line
	ldi		r16, lcd_Home
	call	lcd_write_instruction_4d

	pop		r16
	inc		r16
	call	OSMenuGetEntryByIndex

	ldi		r16, lcd_LineTwo
	call	lcd_write_string_4d_code
	

os_menu_display_ret:
	pop		ZL
	pop		ZH

	pop		r25
	pop		r17
	pop		r16
	ret

; Name:     menu_display
; Purpose:  Responsible for displaying menu entries on LCD
; Exit:     no parameters
; Notes:   
OSMenuKeyboardHandler:
	push	r16

	call	GetCurrentButton

	cpi		r16, BTN_SELECT
	breq	os_menu_kb_callback

	cpi		r16, BTN_UP
	breq	os_menu_kb_up

	cpi		r16, BTN_DOWN
	breq	os_menu_kb_down

	rjmp	os_menu_kb_ret

os_menu_kb_callback:
	lds		r16, os_current_index
	call	OSMenuCallCallbackByIndex

	rjmp	os_menu_kb_ret
os_menu_kb_up:
	call	OSMenuDecrement
	rjmp	os_menu_kb_display
os_menu_kb_down:
	call	OSMenuIncrement
	rjmp	os_menu_kb_display

os_menu_kb_display:
	call	OSMenuDisplay

os_menu_kb_ret:
	pop		r16
	ret

; Name:     OSMenuCallCallbackByIndex
; Purpose:  Returns the callback entry by index represneted in R16
; Exit:     Returns the entry in ZH, ZL registers
; Notes:   
OSMenuCallCallbackByIndex:
	push	r16
	push	r17

	push	ZH
	push	ZL

	; R16 = R16 * 2
	lsl		r16

	ldi		ZH, high( os_menu_callbacks_jumptable )
	ldi		ZL,  low( os_menu_callbacks_jumptable )

	clr		r17
	add		ZL, r16
	adc		ZH, r17

	icall

	pop		ZL
	pop		ZH

	pop		r17
	pop		r16
	ret

; Name:     OSMenuGetEntryByIndex
; Purpose:  Returns the menu entry by index represneted in R16
; Exit:     Returns the entry in ZH, ZL registers
; Notes:   
OSMenuGetEntryByIndex:
	push	r16
	push	r17
	push	r18
	eor		r17, r17 

	ldi		ZH, high( os_menu_entries_jumptable )
	ldi		ZL, low( os_menu_entries_jumptable )

	; R16 = R16 * 3
	mov		r18, r16
	lsl		r16
	add		r16, r18

	add		ZL, r16
	adc		ZH, r17

	icall

	pop		r18
	pop		r17
	pop		r16
	ret

#endif
