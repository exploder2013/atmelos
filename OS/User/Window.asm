/*
 * Display.asm
 *
 *  Created: 26.12.2017 23:13:05
 *   Author: Normunds
 */ 

#ifndef H_DISPLAY
#define H_DISPLAY

.include "OS_Functions\Process.asm"

GetActiveProcessPID:
	lds		r25, active_process_pid
	ret

SetActiveProcessPID:
	sts		active_process_pid, r25
	ret

IsMainProcess:
	push	r24

	call	GetActiveProcessPID
	mov		r24, r25

	call	GetCurrentPID
	cp		r24, r25
	brne	IsActiveProcess_false

IsActiveProcess_true:
	ldi		r25, 1
	rjmp	IsActiveProcess_ret

IsActiveProcess_false:
	clr		r25

IsActiveProcess_ret:
	pop		r24
	ret

#endif
