/*
 * ProcessUser.asm
 *
 *  Created: 26.12.2017 01:01:03
 *   Author: Normunds
 */ 

#ifndef H_PROCESS_USER
#define H_PROCESS_USER

.include "OS_Functions\Process.asm"

; Important: THESE FUNCTIONS NEED TO BE CALLED IN THE CONTEXT OF THE PROCESS

; TODO: Implement return codes
; TODO: Maybe zero out context buffer for the new process to have it always start out with zeroed out registers?

; XH & XL - address of the start of the main function.
CreateProcess:
	; Disable interrupts, as we are messing with SP.
	; Otherwise, an interrupt might fire and we will jump to an garbage address.
	cli
	push	ZH
	push	ZL

	push	r16
	push	r17

	call	GetRunningProcessCount

	; Check if we are not above the limit
	cpi		r25, MAX_PROCESS_COUNT
	brge	CreateProcess_ret

	; Get free PID for the new process
	call	GetFreePID

	; Init PC to point to start of the main function (when it reutrns from timer interrupt)
	call	GetProcessSPActualOffset

	; Save current SP
	in		r16, SPH
	in		r17, SPL

	; Replace current stack with that dummy process, so we can assign values to the stack
	out		SPH, ZH
	out		SPL, ZL

	; Store return address
	push	XL
	push	XH

	; Push dummy registers to decrement stack by 32 (we could do this another way, but performance is not important here)
	pushall
	
	; Save new SP in the buffer
	in		XH, SPH
	in		XL, SPL

	call	GetProcessSP
	st		Z+, XH
	st		Z,  XL

	; Restore current stack back to original
	out		SPH, r16
	out		SPL, r17

	; Add PID to the array
	call	AddProcessPID

	; Increment the running process counter
	push	r25

	call	GetRunningProcessCount
	inc		r25
	call	SetRunningProcessCount

	pop		r25

CreateProcess_ret:
	pop		r17
	pop		r16

	pop		ZL
	pop		ZH

	; Enable interrupts again
	sei 
	ret

ExitProcess:
	; Disable interrupts, as we are messing with SP.
	; Otherwise, an interrupt might fire and we will jump to an garbage address.
	cli

	; Remove PID from the list
	; This will make our OS not to run the process
	call	GetCurrentPID
	call	RemoveProcessPID

	; Unregister Keyboard Interrupt if process had one
	call	UnregisterKeyboardInt

	; Unregister Timer interrupt
	call	UnregisterTimerInt

	; Decrement the running process counter
	call	GetRunningProcessCount
	dec		r25
	call	SetRunningProcessCount

	; Switch the display focus to SYSTEM process ( jump back to menu )
	clr		r25
	call	SetActiveProcessPID

	; Force context switch, as otherwise, the process will still keep running after this
	; process techincally has been terminated.
	; Since this is a interrupt subroutine, it will return and enable interrupts at the same time (reti)
	call	OS_HandleTimerInput

	BREAK ; Should never get here
	ret

; Brings the process to background and SYSTEM process to foreground
; Which means that this process still keeps running, but SYSTEM process
; will be drawing on the screen
MinimiseProcess:
	push	r25

	; Switch the display focus to SYSTEM process
	clr		r25
	call	SetActiveProcessPID

	pop		r25

	; Force context switch to reduce percieved delay
	; TODO: Causes problems that keyboard input gets recorded
	; by the next process immediately if we do immediate context switch.
	call	OS_HandleTimerInput
	ret

; r25 - procID for which process to maximse
MaximiseProcess:
	; Currently the only this to do is to mark it as 'active'.
	; Which gives it permission to draw on screen and keyboard input focus.
	call	SetActiveProcessPID

	; Force context switch to reduce percieved delay
	; TODO: Causes problems that keyboard input gets recorded
	; by the next process immediately if we do immediate context switch.
	call	OS_HandleTimerInput

	ret

; r25 - procID for process to terminate
TerminateProcess:
	; TODO - Implement all features
	cli

	push	r24

	call	IsProcessRunning
	tst		r24

	; Process has not been found, exit
	breq	TerminateProcess_ret
	
	; Remove the process from the running process list
	call	RemoveProcessPID

	; If we have terminated our own process, 
	; do a context switch immediately
	mov		r24, r25
	call	GetCurrentPID

	cp		r24, r25
	brne	TerminateProcess_ret

	pop		r24
	; Do a forced context switch to next process
	call	OS_HandleTimerInput

TerminateProcess_ret:
	; TODO: THIS POP WILL NOT HAPPEN IF ABOVE FUNCTION GETS CALLED!
	; DONT CALL UNTIL THIS IS REMOVED!
	pop		r24

	sei
	ret

; r25 - PID to look for
; r24 - Result ( 0 - false | 1 - true )
IsProcessRunning:

	call	FindPID
	cpi		r24, -1
	breq	process_not_running

process_running:
	ldi		r24, 1
	rjmp	IsProcessRunning_ret

process_not_running:
	inc		r24

IsProcessRunning_ret:
	ret

; XH & XL - address of the start of the keyboard interrupt
RegisterKeyboardInt:
	; Disable interrupts, as we are messing with SP.
	; Otherwise, an interrupt might fire and we will jump to an garbage address.
	cli

	push	r25
	push	ZH
	push	ZL
	
	; Get current PID, for functions
	call	GetCurrentPID

	; Get keyboard subroutine offset
	call	GetProcessKeyboardInt

	; Store our address in the array
	st		Z+, XH
	st		Z,  XL

	pop		ZL
	pop		ZH
	pop		r25
	; Enable interrupts again
	sei 
	ret

UnregisterKeyboardInt:
	push	r25
	push	ZH
	push	ZL
	
	; Get current PID, for functions
	call	GetCurrentPID

	; Get keyboard subroutine offset
	call	GetProcessKeyboardInt

	clr		r25
	; Store our address in the array
	st		Z+, r25
	st		Z,  r25

	pop		ZL
	pop		ZH
	pop		r25

	ret

; XH & XL - address of the start of the keyboard interrupt
; r25 - overflow time at which to fire
RegisterTimerInt:
	; Disable interrupts, as we are messing with SP.
	; Otherwise, an interrupt might fire and we will jump to an garbage address.
	cli
	push	r24
	push	r25

	push	ZH
	push	ZL
	

	; Load max overflow limit
	lds		r24, max_overflow

	; Check if overflow != PROCESS_TIMER_NOT_ACTIVE
	; If so, replace it with whatever we got passed
	cpi		r24, PROCESS_TIMER_NOT_ACTIVE
	breq	RegisterTimerInt_register_max_overflow

	; Compare if current > max
	cp		r24, r25
	brsh	RegisterTimerInt_register
	
	; Register new highest overflow count
RegisterTimerInt_register_max_overflow:
	sts		max_overflow, r25

RegisterTimerInt_register:
	mov		r24, r25

	; Get current PID
	call	GetCurrentPID

	; Get keyboard overflow offset
	call	GetProcessTimerOverflow

	; Store overflow time
	st		Z, r24

	; Get keyboard subroutine offset
	call	GetProcessTimerInt

	; Store our address in the array
	st		Z+, XH
	st		Z,  XL

	pop		ZL
	pop		ZH

	pop		r25
	pop		r24
	; Enable interrupts again
	sei 
	ret

UnregisterTimerInt:
	cli	

	push	r25
	push	r24

	push	ZH
	push	ZL
	
	; Get current PID
	call	GetCurrentPID

	; Get keyboard overflow offset
	call	GetProcessTimerOverflow

	; Store overflow time
	ldi		r24, PROCESS_TIMER_NOT_ACTIVE
	st		Z, r24

	; Get keyboard subroutine offset
	call	GetProcessTimerInt

	; Store our address in the array
	clr		r24

	st		Z+, r24
	st		Z,  r24

	; Set maximum overflow to next highest
	; TODO

	pop		ZL
	pop		ZH

	pop		r24
	pop		r25

	sei
	ret

#endif
