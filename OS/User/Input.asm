/*
 * Input.asm
 *
 *  Created: 26.12.2017 23:55:13
 *   Author: Normunds
 */ 

#ifndef H_KEYBOARD_INPUT
#define H_KEYBOARD_INPUT

.dseg
	input_read:		.byte 1
	input_reset:	.byte 1
; This file is here to implement debouncing algorithms or 
; to achieve ways to get consistant keyboard input

.cseg

; Used to block input for one debounce cycle
; Could be useful when switching between contexts of two processes
; so other process doesn't get the old input
BlockInput:
	push	r16
	
	ldi		r16, 1
	sts		input_reset, r16

	pop		r16
	ret

DebounceInput:
	push	r16
	
	lds		r16, input_reset
	tst		r16
	breq	DebounceInput_debounce

DebounceInput_reset:
	; Button has been hit recently, reset the counter
	clr		r16
	sts		input_reset, r16
	rjmp	DebounceInput_ret


DebounceInput_debounce:
	clr		r16
	sts		input_read, r16

DebounceInput_ret:
	pop		r16
	ret

GetCurrentButton:
	; If input was read, we mark the next read as invalid
	; until timer marks it valid again.
	; This solves the problem of getting too many key inputs for a specific time frame

	lds		r16, input_read
	tst		r16
	breq	GetCurrentButton_read

GetCurrentButton_invalid:
	; Last input was read in too short of a timeframe, return BTN_NONE
	ldi		r16, BTN_NONE
	rjmp	GetCurrentButton_ret

GetCurrentButton_read:
	; Mark input as invalid
	ldi		r16, 1
	sts		input_reset, r16
	sts		input_read,  r16
	
	lds		r16, btn_state
	

GetCurrentButton_ret:
	ret

#endif
