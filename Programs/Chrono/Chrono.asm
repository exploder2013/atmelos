/*
 * Chrono.asm
 *
 *  Created: 10.12.2017 22:59:55
 *   Author: Normunds
 */ 

#ifndef H_CHRONO
#define H_CHRONO

.include "..\OS\OS_Functions\Memory.asm"



.dseg
	; Split time in diffirent sections to make referencing to it easier.
	chrono_hours:		.byte 1
	chrono_minutes:		.byte 1
	chrono_seconds:		.byte 1
	chrono_miliseconds:	.byte 1

	chrono_isEnabled:	.byte 1		; It is possible to stop/start stopwatch
	chrono_exit:		.byte 1		; Tells to terminate the process
	
	; Time String
	.equ					CHRONO_TIME_SIZE	= 12
	chrono_time:			.byte CHRONO_TIME_SIZE		;"hh:mm:ss:ms" Used as the "save time" feature for chrono
	chrono_saved_time:		.byte CHRONO_TIME_SIZE		; Saved time, if user presses a button, this will be displayed

	; Space to display it on middle of LCD for both lines
	.equ		lcd_LineOne_chrono_time_offset	= lcd_LineOne + 2
	.equ		lcd_LineTwo_chrono_time_offset	= lcd_LineTwo + 2 

	; Offsets to make time locations easier to reference
	.equ		chrono_time_offset_hours	=	chrono_time + 0
	.equ		chrono_time_offset_minutes	=	chrono_time + 3
	.equ		chrono_time_offset_seconds	=	chrono_time + 6
	.equ		chrono_time_offset_ms		=	chrono_time + 9

	; Timer
	.equ		CHRONO_OVERFLOW_TIME			= 10

.cseg
	
; Name:     get_miliseconds
; Purpose:  Gets the current miliseconds
; Exit:     Returns the miliseconds in r16 register
; Notes:   
.macro ChronoGetMilliseconds
	lds r16, chrono_miliseconds
.endmacro

.macro ChronoGetSeconds
	lds r16, chrono_seconds
.endmacro

.macro ChronoGetMinutes
	lds r16, chrono_minutes
.endmacro

.macro ChronoGetHours
	lds r16, chrono_hours
.endmacro

.macro ChronoStoreAndConvAsAscii
	push	XH
	push	XL

	ldi		XH, high( @0 )
	ldi		XL,  low( @0 )

	sts		@1, r16				; Store new time
	call	ChronoStoreAsAscii	; Convert it to ascii and store it.

	pop		XL
	pop		XH
.endmacro

.macro ChronoItoa
	subi	@0, -48
	subi	@1, -48
.endmacro

ChronoGetTimeAscii:
	ldi		ZH, high(chrono_time)
	ldi		ZL,  low(chrono_time)
	ret

ChronoGetSavedTimeAscii:
	ldi		ZH, high(chrono_saved_time)
	ldi		ZL,  low(chrono_saved_time)
	ret


ChronoStoreAsAscii:
	push	r25
	push	r24
	
	; Get time high:low part
	mov		r25, r16
	ldi		r24, 10
	call	div8u

	; Convert them to ascii representation
	ChronoItoa r25, r24	

	st		X+, r24
	st		X, r25
		
	; Reset X to its original position
	sbiw	XH:XL, 1

	pop		r24
	pop		r25
	ret

; Name:     ChronoSetMilliseconds
; Purpose:  Adjust clock miliseconds.
; Entry:    r16 storing the new miliseconds value to replace
; Exit:     no parameters
; Notes:    Function adjusts seconds if miliseconds > 10 and resets miliseconds back to 0.
ChronoSetMilliseconds:
	push	r16

	; Check if ms >= 10
	cpi		r16, 10
	brlo	chrono_adjust_miliseconds

chrono_inc_seconds:

	; Increment seconds
	ChronoGetSeconds
	inc		r16		
	call	ChronoSetSeconds

	; Reset	miliseconds
	clr		r16

chrono_adjust_miliseconds:
	ChronoStoreAndConvAsAscii chrono_time_offset_ms, chrono_miliseconds

	pop		r16
	ret

; Name:     ChronoSetSeconds
; Purpose:  Adjust clock miliseconds.
; Entry:    r16 storing the new miliseconds value to replace
; Exit:     no parameters
; Notes:    Function adjusts seconds if miliseconds > 10 and resets miliseconds back to 0.
ChronoSetSeconds:
	push	r16

	; Check if s >= 60
	cpi		r16, 60
	brlo	chrono_adjust_seconds

chrono_inc_minutes:

	; Increment minutes
	ChronoGetMinutes
	inc		r16		
	call	ChronoSetMinutes

	; Reset	seconds
	clr		r16

chrono_adjust_seconds:
	ChronoStoreAndConvAsAscii chrono_time_offset_seconds, chrono_seconds

	pop		r16
	ret

; Name:     ChronoSetMinutes
; Purpose:  Adjust clock miliseconds.
; Entry:    r16 storing the new miliseconds value to replace
; Exit:     no parameters
; Notes:    Function adjusts seconds if miliseconds > 10 and resets miliseconds back to 0.
ChronoSetMinutes:
	push	r16

	; Check if s >= 60
	cpi		r16, 60
	brlo	chrono_adjust_minutes

chrono_inc_hours:

	; Increment hours
	ChronoGetHours
	inc		r16		
	call	ChronoSetHours

	; Reset	minutes
	clr		r16

chrono_adjust_minutes:
	ChronoStoreAndConvAsAscii chrono_time_offset_minutes, chrono_minutes

	pop		r16
	ret

; Name:     ChronoSetHours
; Purpose:  Adjust clock miliseconds.
; Entry:    r16 storing the new miliseconds value to replace
; Exit:     no parameters
; Notes:    Function adjusts seconds if miliseconds > 10 and resets miliseconds back to 0.
ChronoSetHours:
	push	r16

	; Check if s >= 60
	cpi		r16, 24
	brlo	chrono_adjust_hours

chrono_reset_hours:

	; Reset hours
	clr		r16		
	call	ChronoSetHours

chrono_adjust_hours:
	ChronoStoreAndConvAsAscii chrono_time_offset_hours, chrono_hours

	pop		r16
	ret

; Name:     Initilises all variables of chronometer.
; Purpose:  Makes sure they are well defined.
ChronoInit:
	push	r16
	push	ZH
	push	ZL

	clr		r16
	sts		chrono_miliseconds	, r16
	sts		chrono_seconds		, r16
	sts		chrono_minutes		, r16
	sts		chrono_hours		, r16

	; Set isEnabled to FALSE
	sts		chrono_isEnabled	, r16

	; Zero saved time string
	ldi		ZH, high( chrono_saved_time )
	ldi		ZL, low( chrono_saved_time )

	ldi		r16, CHRONO_TIME_SIZE
	call	memclr

	; Zero time string
	ldi		ZH, high( chrono_time )
	ldi		ZL, low( chrono_time )

	ldi		r16, CHRONO_TIME_SIZE
	call	memclr
	
	; Fill in time string with ':' symbols
	ldi		r16, ':'

	adiw	ZH:ZL, 2
	st		Z, r16

	adiw	ZH:ZL, 3
	st		Z, r16

	adiw	ZH:ZL, 3
	st		Z, r16

	; Initilise time string with actual numbers
	call	ConvertChronoAscii

	pop		ZL
	pop		ZH
	pop		r16
	ret

; Name:     The main loop for chronometer.
; Purpose:  Initilises chrono, starts counting
ChronoMain:
	; Initilise all variables
	call	ChronoInit

	; Register keyboard interrupt
	ldi		XH, high(ChronoKeyboardHandler)
	ldi		XL,  low(ChronoKeyboardHandler)
	call	RegisterKeyboardInt

	; Register timer interrupt
	ldi		XH, high(ChronoTimerHandler)
	ldi		XL, low(ChronoTimerHandler)
	ldi		r25, 6
	call	RegisterTimerInt

	chrono_loop:	
		call	ChronoDisplay

		; Redraw the screen every 10ms
		ldi		r16, 10
		call	delay_ms

		lds		r16, chrono_exit
		tst		r16
		brne	chrono_terminate

		rjmp	chrono_loop
	
	chrono_terminate:

		; TODO: Implement the ability to terminate process from 
		;		keyboard interrupt
		clr		r16
		sts		chrono_exit, r16
		call	ExitProcess

	BREAK	; Should not get here!
	ret

; Name:     chrono_reset
; Purpose:  Restores the state of chrono to new one.
ChronoReset:
	push	r16

	; Set all times to 0
	clr		r16

	call	ChronoSetMilliseconds
	call	ChronoSetSeconds
	call	ChronoSetMinutes
	call	ChronoSetHours

	pop		r16
	ret

; Name:     display_chrono
; Purpose:  Display chronometer on LCD.
ChronoDisplay:
	push	r16
	push	r25

chrono_display_time:
	; Check if Chrono is main process (in focus)
	call	IsMainProcess
	tst		r25
	breq	chrono_display_ret

	; Clear screen
	ldi		r16, lcd_Clear
	call	lcd_write_instruction_4d

	; Get time string
	call	ChronoGetTimeAscii

	; Display time
	ldi		r16, lcd_LineOne_chrono_time_offset
	call	lcd_write_string_4d_flash

	; Get saved time (if user has saved it)
	call	ChronoGetSavedTimeAscii

	; Display it
	ldi		r16, lcd_LineTwo_chrono_time_offset
	call	lcd_write_string_4d_flash

chrono_display_ret:
	pop		r25
	pop		r16
	ret

; Name:     convert_chrono_ascii
; Purpose:  Converts the numberic to ascii format and stores it in allocated time buffer.
; Entry:    Uses no paramaters
; Exit:     no parameters
; Notes:    
ConvertChronoAscii:
	push	r16

	ChronoGetMilliseconds
	call ChronoSetMilliseconds

	ChronoGetSeconds
	call ChronoSetSeconds

	ChronoGetMinutes
	call ChronoSetMinutes

	ChronoGetHours
	call ChronoSetHours

	pop		r16
	ret

; Name:     ChronoSaveTime
; Purpose:  Saves the current time in second saved time buffer for display
; Entry:    Uses no paramaters
; Exit:     no parameters
; Notes:    
ChronoSaveTime:
	push	r16
	
	push	XH
	push	XL
	push	ZH
	push	ZL

	call	ChronoGetTimeAscii
	mov		XH, ZH
	mov		XL, ZL

	call	ChronoGetSavedTimeAscii
	ldi		r16, CHRONO_TIME_SIZE
	call	memcpy

	pop		ZL
	pop		ZH
	pop		XL
	pop		XH

	pop		r16
	ret

; Name:     chrono_kb_handle
; Purpose:  Responsible for keyboard inputs when chrono is active
; Exit:     no parameters
; Notes:   
ChronoKeyboardHandler:
	; TODO: Add chrono reset button
	push	r16

	; Get current button pressed
	call	GetCurrentButton

	cpi		r16, BTN_SELECT
	breq	chrono_kb_change_mode

	cpi		r16, BTN_DOWN
	breq	chrono_kb_exit_process

	cpi		r16, BTN_LEFT
	breq	chrono_kb_left

	cpi		r16, BTN_RIGHT
	breq	chrono_kb_right

	cpi		r16, BTN_UP
	breq	chrono_kb_up

	rjmp	chrono_kb_ret

chrono_kb_change_mode:		; Jump back to OS

	call	MinimiseProcess ; Just minimise, keep running in background
	rjmp	chrono_kb_ret

chrono_kb_exit_process:		; Exit out of the process
	ldi		r16, 1
	sts		chrono_exit, r16

	rjmp	chrono_kb_ret	; Shouldn't get here!

chrono_kb_up:				; Reset timer
	call	ChronoReset
	rjmp	chrono_kb_ret


chrono_kb_right:	; Save current time
	call	ChronoSaveTime
	rjmp	chrono_kb_disp


chrono_kb_left:				; Stop/Start stopwatch
	lds		r16, chrono_isEnabled
	tst		r16

	brne	chrono_kb_stop

chrono_kb_start:		; Start chrono running again
	inc		r16
	sts		chrono_isEnabled, r16
	
	rjmp	chrono_kb_ret
chrono_kb_stop:			; Stop chrono from running
	dec		r16
	sts		chrono_isEnabled, r16

	rjmp	chrono_kb_ret

chrono_kb_disp:
	call	ChronoDisplay

chrono_kb_ret:
	pop		r16
	ret

ChronoTimerHandler:
	push	r16

	; Check if chrono is enabled
	lds		r16, chrono_isEnabled
	tst		r16

	; Exit if timer is disabled
	breq	ChronoTimerHandler_end

	; Should avoid sleeping in timer, as this slows other processes down
	; Wait for 10ms extra (6.127 overflow coeficient)
	;ldi	r16, 10
	;call	delay_ms

	; Increment time
	ChronoGetMilliseconds
	inc		r16
	call	ChronoSetMilliseconds

ChronoTimerHandler_end:
	pop		r16
	ret

#endif
