/*
 * Alarm.asm
 *
 *  Created: 11.12.2017 16:50:43
 *   Author: Normunds
 */ 

#ifndef H_CLOCK_ALARM
#define H_CLOCK_ALARM

.include "..\Programs\Clock\Clock.asm"

.equ	alarm_blink_phases		= 5

; Name:     alarm_start
; Purpose:  Sets up the mode for alarm
; Exit:     no parameters
; Notes:   
SetAlarmHours:
	sts		alarm_h, r16
	ret

SetAlarmMinutes:
	sts		alarm_m, r16
	ret

SetAlarmSeconds:
	sts		alarm_s, r16
	ret

; Name:     alarm_init
; Purpose:  Sets up the paramaters for alarm
; Exit:     no parameters
; Notes:    
AlarmInit:
	push	r16
	clr		r16

	call	SetAlarmSeconds
	call	SetAlarmMinutes
	call	SetAlarmHours

	; Make alarm not set
	sts		alarm_isEnabled, r16
	sts		alarm_blink_state_current, r16

	; Alarm is not ongoing.
	sts		alarm_isOngoing, r16

	; Alarm blink phases (how many times it should blink)
	ldi		r16, alarm_blink_phases
	sts		alarm_blink_state, r16

	pop		r16
	ret

; Name:     AlarmStart
; Purpose:  Sets up the mode for alarm
; Exit:     no parameters
; Notes:   
AlarmStart:	
	push	r16
	push	r17

	GetSeconds
	lds		r17, alarm_s

	sts		alarm_s, r16
	sts		seconds, r17

	GetMinutes
	lds		r17, alarm_m

	sts		alarm_m, r16
	sts		minutes, r17

	GetHours
	lds		r17, alarm_h

	sts		alarm_h, r16
	sts		hours, r17

	pop		r17
	pop		r16
	ret

; Name:     alarm_end
; Purpose:  Restores back the old state of clock
; Exit:     no parameters
; Notes:   
AlarmEnd:
	push	r16
	; As alarm start swaps the states, thats all we want
	call	AlarmStart

	; For now, just enable it. TODO: Not needed anymore as we have ToggleAlarm in menu
	;clr		r16
	;inc		r16
	;sts		alarm_isEnabled, r16

	pop		r16
	ret

; Name:     AlarmKeyboardHandler
; Purpose:  Handles keyboard events for alarm
; Exit:     no parameters
; Notes:   
AlarmKeyboardHandler:
	push	r16
	; Alarm keyboard handler mostly inherits all buttons from clock, except
	; SELECT button, as it will be used to save the alarm time
	call	GetCurrentButton

	cpi		r16, BTN_SELECT
	brne	handle_clock

	; Save alarm time, reset back to clock
handle_alarm:
	call	AlarmEnd

	; Swap state back to menu
	ldi		r16, MENU_SELECT
	sts		menu_state, r16

	; Adjust to menu timings
	lds		r16, menu_timing
	sts		time_delay, r16

	rjmp	alarm_kb_end
handle_clock:
	call	ClockKeyHandler
	call	ClockDisplay

alarm_kb_end:
	pop		r16
	ret

#endif
