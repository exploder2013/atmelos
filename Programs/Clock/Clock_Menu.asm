/*
 * Menu.asm
 *
 *  Created: 10.12.2017 21:36:42
 *   Author: Normunds
 */ 

#ifndef H_CLOCK_MENU
#define H_CLOCK_MENU

.include "..\Programs\Clock\Clock_MenuCallbacks.asm"

.dseg
	
	current_index:	.byte 1
	max_index:		.byte 1

.equ menu_entry_count		= 5

.cseg
menu_entries:
	menu_go_clock:		.db "Go To Clock", 0
	menu_set_clock:		.db "Set Clock", 0
	menu_set_alarm:		.db "Set Alarm", 0
	menu_toggle_alarm:	.db "Toggle Alarm", 0, 0
	menu_minimise_txt:	.db "Minimise", 0, 0
	menu_exit_txt:		.db "Exit", 0, 0

; Jumptable of menu entires (3 bytes)
menu_entries_jumptable:
	ldi		ZH, high(menu_go_clock)
	ldi		ZL, low (menu_go_clock)
	ret
	ldi		ZH, high(menu_set_clock)
	ldi		ZL, low (menu_set_clock)
	ret
	ldi		ZH, high(menu_set_alarm)
	ldi		ZL, low (menu_set_alarm)
	ret
	ldi		ZH, high(menu_toggle_alarm)
	ldi		ZL, low (menu_toggle_alarm)
	ret
	ldi		ZH, high(menu_minimise_txt)
	ldi		ZL, low (menu_minimise_txt)
	ret
	ldi		ZH, high(menu_exit_txt)
	ldi		ZL, low (menu_exit_txt)
	ret

; Jumptable of menu callbacks (1 byte)
menu_callbacks_jumptable:
	jmp		callback_switch_clock
	jmp		callback_set_clock
	jmp		callback_set_alarm
	jmp		callback_toggle_alarm
	jmp		callback_menu_minimise
	jmp		callback_menu_exit

; Name:     menu_init
; Purpose:  Initilises all menu variables.
; Exit:     no parameters
; Notes:   
MenuInit:
	push	r16

	clr		r16
	sts		current_index, r16

	ldi		r16, menu_entry_count
	sts		max_index, r16

	pop		r16
	ret

; Name:     menu_display
; Purpose:  Responsible for displaying menu entries on LCD
; Exit:     no parameters
; Notes:   
MenuIncrement:
	push	r16
	push	r17

	lds		r16, current_index
	lds		r17, max_index

	; Check if we have reached the last entry. If so, reset index.
	cp		r16, r17
	breq	menu_increment_reset

menu_increment_add:
	inc		r16
	rjmp	menu_increment_ret

menu_increment_reset:
	clr		r16

menu_increment_ret:
	sts		current_index, r16

	pop		r17
	pop		r16
	ret

; Name:     menu_display
; Purpose:  Responsible for displaying menu entries on LCD
; Exit:     no parameters
; Notes:   
MenuDecrement:
	push	r16
	push	r17

	lds		r16, current_index
	lds		r17, max_index

	; Check if we have reached the first entry. If so, set index to last entry.
	cpi		r16, 0
	breq	menu_decrement_set_last

menu_decrement_dec:
	dec		r16
	rjmp	menu_decrement_ret

menu_decrement_set_last:
	mov		r16, r17

menu_decrement_ret:
	sts		current_index, r16

	pop		r17
	pop		r16
	ret

; Name:     menu_display
; Purpose:  Responsible for displaying menu entries on LCD
; Exit:     no parameters
; Notes:   
MenuDisplay:
	push	r16
	push	r17

	push	ZH
	push	ZL

	ldi		r16, lcd_Clear
	call	lcd_write_instruction_4d

	lds		r16, current_index
	lds		r17, max_index

	; Decide whether there is two or one entry to display ( if at the end of menu )
	sub		r17, r16
	tst		r17

	; If r17 == 0, then display one
	brne	display_double

display_single:
	call	MenuGetEntryByIndex

	ldi		r16, lcd_LineOne
	call	lcd_write_string_4d_code

	ldi		r16, '<'
	call	lcd_write_character_4d

	rjmp	menu_display_ret
display_double:
	push	r16
	call	MenuGetEntryByIndex

	ldi		r16, lcd_LineOne
	call	lcd_write_string_4d_code

	ldi		r16, '<'
	call	lcd_write_character_4d

	; Need this because otherwise, the other line gets written on the same line
	ldi		r16, lcd_Home
	call	lcd_write_instruction_4d

	pop		r16
	inc		r16
	call	MenuGetEntryByIndex

	ldi		r16, lcd_LineTwo
	call	lcd_write_string_4d_code
	

menu_display_ret:
	pop		ZL
	pop		ZH

	pop		r17
	pop		r16
	ret

; Name:     menu_call_callback_by_index
; Purpose:  Returns the callback entry by index represneted in R16
; Exit:     Returns the entry in ZH, ZL registers
; Notes:   
MenuCallCallbackByIndex:
	push	r16
	push	r17

	push	ZH
	push	ZL

	; R16 = R16 * 2
	lsl		r16

	ldi		ZH, high( menu_callbacks_jumptable )
	ldi		ZL, low( menu_callbacks_jumptable )

	clr		r17
	add		ZL, r16
	adc		ZH, r17

	icall

	pop		ZL
	pop		ZH

	pop		r17
	pop		r16
	ret

; Name:     MenuGetEntryByIndex
; Purpose:  Returns the menu entry by index represneted in R16
; Exit:     Returns the entry in ZH, ZL registers
; Notes:   
MenuGetEntryByIndex:
	push	r16
	push	r17
	push	r18
	eor		r17, r17 

	ldi		ZH, high( menu_entries_jumptable )
	ldi		ZL, low( menu_entries_jumptable )

	; R16 = R16 * 3
	mov		r18, r16
	lsl		r16
	add		r16, r18

	add		ZL, r16
	adc		ZH, r17

	icall

	pop		r18
	pop		r17
	pop		r16
	ret

; Name:     menu_display
; Purpose:  Responsible for displaying menu entries on LCD
; Exit:     no parameters
; Notes:   
MenuKeyboardHandler:
	push	r16

	call	GetCurrentButton

	cpi		r16, BTN_SELECT
	breq	menu_kb_callback

	cpi		r16, BTN_UP
	breq	menu_kb_up

	cpi		r16, BTN_DOWN
	breq	menu_kb_down

	rjmp	menu_kb_ret

menu_kb_callback:
	lds		r16, current_index

	; Call the callback
	call	MenuCallCallbackByIndex
	rjmp	menu_kb_ret

menu_kb_up:
	call	MenuDecrement
	rjmp	menu_kb_disp
menu_kb_down:
	call	MenuIncrement
	rjmp	menu_kb_disp

menu_kb_disp:
	call	MenuDisplay

menu_kb_ret:
	pop		r16
	ret

#endif
