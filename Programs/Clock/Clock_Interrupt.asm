/*
 * Clock_Interrupt.asm
 *
 *  Created: 28.12.2017 02:47:47
 *   Author: Normunds
 */ 

#ifndef H_CLOCK_INTERRUPT
#define H_CLOCK_INTERRUPT

.include "..\Programs\Clock\Clock_Global.asm"

.cseg

; ------------------------- JUMPTABLES ------------------------------------
; Jumptable for handleKeyboardInput (1 byte)
handleKeyboardInput_jumptable:
	jmp MenuKeyboardHandler
	jmp ClockKeyboardHandler
	jmp AlarmKeyboardHandler

 ; Name:    handleKeyboardInput 
; Purpose:  Handles the keybaord interrupts and calls the specified interrupt function for the current mode.
; Entry:    Uses no arguments
; Notes:  
HandleKeyboardInput:
	push	r16
	push	r17
	push	ZH
	push	ZL

	lds		r16, menu_state
	clr		r17

	;		r16 = r16 * 2
	lsl		r16

	ldi		ZH, high(handleKeyboardInput_jumptable)
	ldi		ZL, low(handleKeyboardInput_jumptable)

	add		ZL, r16
	adc		ZH,	r17

	icall

handle_kb_ret:
	pop		ZL
	pop		ZH

	pop		r17
	pop		r16
	ret

#endif
