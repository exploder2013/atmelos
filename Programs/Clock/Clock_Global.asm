/*
 * Clock_Global.asm
 *
 *  Created: 28.12.2017 02:21:12
 *   Author: Normunds
 */ 

#ifndef H_CLOCK_GLOBAL
#define H_CLOCK_GLOBAL

.dseg
	
	; Menu States
	.equ MENU_SELECT	= 0
	.equ MENU_CLOCK		= 1
	.equ MENU_ALARM		= 2

	.equ MENU_TIMING		= 20

	.equ CLOCK_TIMING		= 30
	.equ CLOCK_SET_TIMING	= 30

	.equ CLOCK_TIMER_CALLBACK	= 0
	.equ MENU_TIMER_CALLBACK	= 1

	menu_state:			.byte 1 ; Contains the current menu state to draw

	time_delay:			.byte 1 ; Contains the time to wait for in 40000Hz per each value
								; So a value of 100 will wait for 1600000Hz, or 1 second.

	clock_exit:			.byte 1
	clock_minimise:		.byte 1

.cseg

	ClockGlobalInit:
		push	r16

		clr		r16
		sts		menu_state, r16

		sts		clock_minimise, r16
		sts		clock_exit, r16

		; Set our first initial timing (depends on the state we start on)
		ldi		r16, MENU_TIMING
		sts		time_delay, r16

		pop		r16
	ret

#endif
