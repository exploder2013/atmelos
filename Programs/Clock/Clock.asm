;/*
; * clock.inc
; *
; *  Created: 27.11.2017 16:38:14
; *   Author: Normunds
; */ 

#ifndef H_CLOCK
#define H_CLOCK

.dseg
	; Split time in diffirent sections to make referencing to it easier.
	hours:		.byte 1
	minutes:	.byte 1
	seconds:	.byte 1

	day:		.byte 1
	month:		.byte 1
	year:		.byte 2

	;	Alarm time
	alarm_h:	.byte 1
	alarm_m:	.byte 1
	alarm_s:	.byte 1

	alarm_isEnabled:	.byte 1
	alarm_isOngoing:	.byte 1		; Is alarm currently ongoing?
	alarm_blink_state:	.byte 1		; Blink/Don't Blink character

	alarm_blink_state_current:	.byte 1	

	clock_isEnabled:	.byte 1
	clock_blink_state:	.byte 1	; Used for the blink function when setting time

	; Used for input with keyboard
	kb_time_offset:	.byte 1

	.equ		kb_time_offset_seconds		=	0
	.equ		kb_time_offset_minutes		=	1
	.equ		kb_time_offset_hours			=	2
	.equ		kb_time_offset_days			=	3
	.equ		kb_time_offset_months		=	4
	.equ		kb_time_offset_year			=	5

	; Data holding clock in a textual representation.
	.equ		time_size					=	9
	.equ		date_size					=	12
	.equ		lcd_LineOne_offset_time		=	lcd_LineOne + 4
	.equ		lcd_LineOne_offset_date		=	lcd_LineTwo + 2

	time:		.byte time_size  ; "hh:mm:ss"			; empty spaces at start for centring
	date:		.byte date_size  ; "dd/mmm/yyyy"		; empty spaces at start for centring

	.equ		time_offset_hours	=	time + 0
	.equ		time_offset_minutes	=	time + 3
	.equ		time_offset_seconds	=	time + 6

	.equ		date_offset_day		=	date + 0
	.equ		date_offset_month	=	date + 3
	.equ		date_offset_year	=	date + 7

	; Timer
	.equ		CLOCK_OVERFLOW_TIME			= 10

.cseg
; ============================== Constants ======================

; Months -> Used for jump/offset table at convert_month_textual
jan: .db "Jan", 0
feb: .db "Feb", 0
mar: .db "Mar", 0
apr: .db "Apr", 0
may: .db "May", 0
jun: .db "Jun", 0
jul: .db "Jul", 0
aug: .db "Aug", 0
sep: .db "Sep", 0
oct: .db "Oct", 0
nov: .db "Nov", 0
_dec: .db "Dec", 0	; Added "_" because dec of dec instruction

; ============================== Clock Macros ======================

; ============================== Getters ======================

; Name:     GetYear
; Purpose:  Gets the current year
; Exit:     Returns the year in ZH, ZL registers
; Notes:   
.macro GetYear
	lds ZH, year
	lds ZL, year + 1
.endmacro

; Name:     GetMonth
; Purpose:  Gets the current month
; Exit:     Returns the month in r16 register
; Notes:   
.macro GetMonth
	lds r16, month
.endmacro

; Name:     GetDay
; Purpose:  Gets the current day
; Exit:     Returns the month in r16 register
; Notes:   
.macro GetDay
	lds r16, day
.endmacro

; Name:     GetHours
; Purpose:  Gets the current hours
; Exit:     Returns the hours in r16 register
; Notes:   
.macro GetHours
	lds r16, hours
.endmacro

; Name:     GetMinutes
; Purpose:  Gets the current minutes
; Exit:     Returns the minutes in r16 register
; Notes:   
.macro GetMinutes
	lds r16, minutes
.endmacro

; Name:     GetSeconds
; Purpose:  Gets the current seconds
; Exit:     Returns the seconds in r16 register
; Notes:   
.macro GetSeconds
	lds r16, seconds
.endmacro


.macro ClockStoreAndConvAsAscii
	push	XH
	push	XL

	ldi		XH, high( @0 )
	ldi		XL,  low( @0 )

	sts		@1, r16				; Store new time
	call	ClockStoreAsAscii	; Convert it to ascii and store it.

	pop		XL
	pop		XH
.endmacro

.macro ClockItoa
	subi	@0, -48
	subi	@1, -48
.endmacro

ClockStoreAsAscii:
	push	r25
	push	r24
	
	; Get time high:low part
	mov		r25, r16
	ldi		r24, 10
	call	div8u

	; Convert them to ascii representation
	ClockItoa r25, r24	

	st		X+, r24
	st		X, r25
		
	; Reset X to its original position
	sbiw	XH:XL, 1

	pop		r24
	pop		r25
	ret

; ============================== Clock Subroutines ======================

; Name:     ClockInit
; Purpose:  Sets all values to 0.
; Exit:     no parameters
; Notes:   

ClockInit:
	push	r16
	push	r17

	push	ZH
	push	ZL
	
	clr		r16
	sts		kb_time_offset, r16

	; Set the isEnabled byte to TRUE and blink to TRUE
	inc		r16
	sts		clock_isEnabled, r16
	sts		clock_blink_state, r16
	dec		r16

	; Set the inital time

	; Year
	ldi		r16, 19
	sts		year, r16
	ldi		r16, 99
	sts		year + 1, r16

	; Month
	ldi		r16, 11
	sts		month, r16

	; Day
	ldi		r16, 30
	sts		day, r16
	
	; Hours
	ldi		r16, 23
	sts		hours, r16
	
	; Minutes
	ldi		r16, 59
	sts		minutes, r16

	; Seconds
	ldi		r16, 50
	sts		seconds, r16


	; Null out time buffer
	ldi		ZH, high( time )
	ldi		ZL, low( time )

	ldi		r16, time_size
	call	memclr

	; Add the ":" characters in time buffer
	ldi		r16, ':'
	adiw	ZH:ZL,  2
	st		Z+, r16

	adiw	ZH:ZL,  2
	st		Z, r16

	; Init time buffer for startup, so we fill out the numbers with initial values
	call	ConvertTimeAscii		

	; Null out date buffer
	ldi		ZH, high( date )
	ldi		ZL, low( date )

	ldi		r16, date_size
	call	memclr

	; Add the "/" characters in date buffer
	ldi		r16, '/'	
	adiw	ZH:ZL,  2
	st		Z+, r16

	adiw	ZH:ZL,  3
	st		Z, r16

	; Init date buffer for startup, so we fill out the numbers/month with initial values
	call	ConvertDateAscii

	clr		r16
	; Make alarm not set
	sts		alarm_isEnabled, r16
	sts		alarm_blink_state_current, r16

	; Alarm is not ongoing.
	sts		alarm_isOngoing, r16

	pop		ZL
	pop		ZH

	pop		r17
	pop		r16
	ret

; ============================== Setters ======================

; Name:     SetSeconds
; Purpose:  Adjust clock seconds.
; Entry:    r16 storing the new seconds value to replace
; Exit:     no parameters
; Notes:    Function adjusts minutes if seconds > 59 and resets seconds back to 0.

SetSeconds:
	push	r16

	; Handle the cases where time could be < 0
	tst		r16
	brge	check_seconds

	; Set it to max time value instead of overflow
	ldi		r16, 59

check_seconds:
	cpi		r16, 60
	brlo	adjust_seconds

inc_minutes:
	GetMinutes
	inc		r16
		
	call	SetMinutes
	eor		r16, r16 ; Reset r16

adjust_seconds:
	ClockStoreAndConvAsAscii time_offset_seconds, seconds

	pop		r16
	ret

; Name:     SetMinutes
; Purpose:  Adjust clock minutes.
; Entry:    r16 storing the new minutes value to replace
; Exit:     no parameters
; Notes:    Function adjusts hours if minutes > 59 and resets minutes back to 0.

SetMinutes:
	push	r16

	; Handle the cases where time could be < 0
	tst		r16
	brge	check_minutes

	; Set it to max time value instead of overflow
	ldi		r16, 59

check_minutes:
	cpi		r16, 60
	brlo	adjust_minutes

inc_hours:
	GetHours
	inc		r16
		
	call	SetHours
	eor		r16, r16 ; Reset r16

adjust_minutes:
	ClockStoreAndConvAsAscii time_offset_minutes, minutes

	pop		r16
	ret

; Name:     SetHours
; Purpose:  Adjust clock hours.
; Entry:    r16 storing the new hours value to replace
; Exit:     no parameters
; Notes:    Function adjusts day if hours > 23 and resets hours back to 0.

SetHours:
	push	r16

	; Handle the cases where time could be < 0
	tst		r16
	brge	check_hours

	; Set it to max time value instead of overflow
	ldi		r16, 23

check_hours:
	cpi		r16, 24
	brlo	adjust_hours

inc_day:
	GetDay
	inc		r16
		
	call	SetDay
	eor		r16, r16 ; Reset r16

adjust_hours:
	ClockStoreAndConvAsAscii time_offset_hours, hours

	pop		r16
	ret

; Name:     SetDay
; Purpose:  Adjust clock day.
; Entry:    r16 storing the new day value to replace
; Exit:     no parameters
; Notes:    Function adjusts month if day >= 30 and resets day back to 0.
;			User can use a jumptable to decide if month has 31 or 30 days, but
;			this function doesn't implement it.
SetDay:
	push	r16

	; Handle the cases where time could be < 0
	tst		r16
	brge	check_day

	; Set it to max time value instead of overflow
	ldi		r16, 30

check_day:
	cpi		r16, 31
	brlo	adjust_day

inc_month:
	GetMonth
	inc		r16
		
	call	SetMonth
	eor		r16, r16 ; Reset r16

adjust_day:
	ClockStoreAndConvAsAscii date_offset_day, day

	pop		r16
	ret

; Name:     SetDay
; Purpose:  Adjust clock day.
; Entry:    r16 storing the new day value to replace
; Exit:     no parameters
; Notes:    Function adjusts year if month >= 13 and resets day back to 0.
;			User can use a jumptable to decide if month has 31 or 30 days, but
;			this function doesn't implement it.
SetMonth:
	push	r16

	; Handle the cases where time could be < 0
	tst		r16
	brge	check_month

	; Set it to max time value instead of overflow
	ldi		r16, 11

check_month:
	cpi		r16, 12
	brlo	adjust_month

inc_year:
	push	ZH
	push	ZL

	GetYear
	inc		ZL
	call	SetYear

	pop		ZL
	pop		ZH
	eor		r16, r16	; Reset r16

adjust_month:
	sts		month, r16				; Store new month
	call	ConvertMonthTextual		; Convert it to ascii and store in date buffer.

	pop		r16
	ret

; Name:     SetYear
; Purpose:  Adjust clock year.
; Entry:    ZH and ZL (hhll format) storing the new year value to replace
; Exit:     no parameters
; Notes:    
SetYear:
	push	r16
	push	ZH
	push	ZL

	cpi		ZL, 100
	brlo	adjust_year

	eor		ZL, ZL		; Reset low side of year

	inc		ZH
	cpi		ZH, 100
	brlo	adjust_year

	eor		ZL, ZL		; Reset high side of year

adjust_year:
	sts		year, ZH
	sts		year + 1, ZL

	push	XH
	push	XL

	ldi		XH, high( date_offset_year )
	ldi		XL, low( date_offset_year )

	mov		r16, ZH
	call	ClockStoreAsAscii	; Convert it to ascii and store in time buffer.

	mov		r16, ZL
	adiw	XH:XL, 2

	call	ClockStoreAsAscii	; Convert it to ascii and store in time buffer.

	pop		XL
	pop		XH
	
	pop		ZL
	pop		ZH
	pop		r16
	ret

; ============================== Utils ======================

; Name:     ConvertTimeAscii
; Purpose:  Converts the numberic to ascii format and stores it in allocated time buffer.
; Entry:    Uses no paramaters
; Exit:     no parameters
; Notes:    
ConvertTimeAscii:
	push	r16

	GetHours
	call SetHours

	GetMinutes
	call SetMinutes

	GetSeconds
	call SetSeconds

	pop		r16
	ret

; Name:     ConvertDateAscii
; Purpose:  Converts the numberic to ascii format and stores it in allocated date buffer.
; Entry:    Uses no paramaters
; Exit:     no parameters
; Notes:    
ConvertDateAscii:
	push	r16

	GetDay
	call SetDay

	GetMonth
	call SetMonth

	GetYear
	call SetYear

	pop		r16
	ret

; Name:     get_time_ascii
; Purpose:  Returns the current time buffer in ascii format
; Entry:    Uses no paramaters
; Exit:     Time buffer will be held in ZH & ZL register
; Notes:   
GetTimeAscii:
	; TODO: Maybe convert to macro instead.

	ldi		ZH, high( time )
	ldi		ZL, low( time )
	ret

; Name:     get_date_ascii
; Purpose:  Returns the current date buffer in ascii format
; Entry:    Uses no paramaters
; Exit:     Date buffer will be held in ZH & ZL register
; Notes:   
GetDateAscii:
	ldi		ZH, high( date )
	ldi		ZL, low( date )
	ret

; Name:     convert_month_textual
; Purpose:  Converts the month to a 3 letter textual representation.
; Entry:    Uses no paramaters
; Notes:   

ConvertMonthTextual:
	push	r16
	push	r17

	push	ZH
	push	ZL

	ldi		ZH, high(jan << 1)
	ldi		ZL, low (jan << 1)

	GetMonth

	; Calculate the offset
	mov		r17, r16	; r16 = r16 * 4
	add		r16, r17
	add		r16, r17
	add		r16, r17 

	eor		r17, r17	; Zero r17 before, to not clear the C flag for ADC.
	add		ZL,  r16
	adc		ZH,  r17	; Deal with overflow

assign_month:
	; Repeat for each 3 characters
	lpm		r16, Z+
	sts		date_offset_month, r16
	lpm		r16, Z+
	sts		date_offset_month + 1, r16
	lpm		r16, Z+
	sts		date_offset_month + 2, r16

convert_month_textual_end:
	pop		ZL
	pop		ZH

	pop		r17
	pop		r16
	ret

; Name:     clock_display
; Purpose:  Displays the current time on LCD. 
;			Blinks the currently selected time if clock is stopped.
; Entry:    Uses no paramaters
; Notes:  
ClockDisplay:
	push	r16

	ldi		r16, lcd_Clear
	call	lcd_write_instruction_4d	

	; If clock is disabled, blink time
	lds		r16, clock_isEnabled
	tst		r16
	brne	clock_display_0

clock_display_blink:
	call	ClockBlinkTime

clock_display_0:
	; Only process alarm is clock is enabled
	call	CheckAlarm

	call	GetTimeAscii

	ldi		r16, lcd_LineOne_offset_time
	call	lcd_write_string_4d_flash

	call	GetDateAscii

	ldi		r16, lcd_LineOne_offset_date
	call	lcd_write_string_4d_flash
	
	pop		r16
	ret

; Name:     check_alarm
; Purpose:  Checks if alarm is set, and blinks if alarm is happening
; Entry:    Uses no paramaters
; Notes:  
CheckAlarm:
	push	r16
	push	r17
	
	lds		r16, clock_isEnabled
	tst		r16
	breq	check_alarm_end

	lds		r16, alarm_isEnabled
	tst		r16
	breq	check_alarm_end		; Alarm not enabled

	lds		r16, alarm_isOngoing
	tst		r16
	brne	check_alarm_process	; Alarm is ongoing

	; Alarm is set, but not ongoing, check time.
	rjmp	check_alarm_check_time

check_alarm_process:
	call	ProcessAlarm
	rjmp	check_alarm_end

check_alarm_check_time:

	GetSeconds
	lds		r17, alarm_s
	cp		r16, r17
	brne	check_alarm_end

	GetMinutes
	lds		r17, alarm_m
	cp		r16, r17
	brne	check_alarm_end

	GetHours
	lds		r17, alarm_h
	cp		r16, r17
	brne	check_alarm_end

	; If we are here, then the time == alarm_time
	ldi		r16, 1
	sts		alarm_isOngoing, r16	; Set alarm as ongoing

	call	ProcessAlarm

check_alarm_end:
	pop		r17
	pop		r16
	ret

; Name:     process_alarm
; Purpose:  If alarm is ongoing, processes it (blinks)
; Entry:    Uses no paramaters
; Notes:  
ProcessAlarm:
	push	r16
	push	r17

	lds		r17, alarm_blink_state
	lds		r16, alarm_blink_state_current

	cp		r16, r17
	brne	alarm_blink

alarm_has_ended:
	clr		r16
	sts		alarm_blink_state_current, r16
	sts		alarm_isOngoing, r16

	; Make sure the display is on after alarm ( it could stop at off state )
	ldi		r16, lcd_DisplayOn
	call	lcd_write_instruction_4d

	rjmp	process_alarm_end

	
alarm_blink:
	inc		r16
	sts		alarm_blink_state_current, r16
	dec		r16

	andi	r16, 1
	tst		r16
	breq	alarm_blink_off

alarm_blink_on:
	ldi		r16, lcd_DisplayOn
	call	lcd_write_instruction_4d

	rjmp	process_alarm_end
alarm_blink_off:
	ldi		r16, lcd_DisplayOff
	call	lcd_write_instruction_4d
	
process_alarm_end:
	pop		r17
	pop		r16
	ret


; Name:     clock_blink_time
; Purpose:  Blinks the currently selected time, so its easier to adjust time
; Entry:    Uses no paramaters
; Notes:  
ClockBlinkTime:
	push	r16
	push	r17
	push	r18

	push	ZH
	push	ZL

	lds		r16, kb_time_offset
	mov		r17, r16

	; Since the offset is a value between 0 - 4, we can multiply it
	; by 3 and go to the right position in time buffer

	; We need to handle time & date buffer differently
	; The offset from 0 - 2 in kb_time_offset represents time buffer (ss:mm:hh)
	; Offsets from 3 - 4 represents date buffer (dd:mmm)
	; As well as month is a 3 characters instead of 2

	cpi		r16, 3
	brge	handle_date

handle_time:
	lsl		r16
	add		r16, r17

	subi	r16, 6
	neg		r16

	ldi		r18, 2

	; Put time buffer in ZH&ZL
	call	GetTimeAscii
	rjmp	blink_check

handle_date:
	; Adjust r16 to be relative to date buffer
	subi	r16, 3
	mov		r17, r16

	; Put date buffer in ZH&ZL
	call	GetDateAscii

	;		r16 = R16 * 3
	lsl		r16
	add		r16, r17

	tst		r16
	brne	handle_date_month

handle_date_day:
	ldi		r18, 2
	rjmp	blink_check

handle_date_month:
	ldi		r18, 3

blink_check:
	; Now check whether we should fill it it erase it.
	lds		r17, clock_blink_state
	tst		r17
	breq	blink_erase

blink_show:
	dec		r17
	sts		clock_blink_state, r17

	; Easiest method to restore time buffer without branches/jumptables is just to
	; convert all the time to its ascii representation again.

	call	ConvertTimeAscii
	call	ConvertDateAscii
	rjmp	blink_end
blink_erase:
	inc		r17
	sts		clock_blink_state, r17

	clr		r17
	add		ZL, r16
	adc		ZH, r17

	; Replace time by empty spaces to achieve the blinking functionality
blink_replace_loop:
	tst		r18
	breq	blink_end
	dec		r18

	ldi		r16, ' '
	st		Z+, r16
	rjmp	blink_replace_loop


blink_end:
	pop		ZL
	pop		ZH

	pop		r18
	pop		r17
	pop		r16
	ret

; Name:     convert_month_textual
; Purpose:  Converts the month to a 3 letter textual representation.
; Entry:    Uses no paramaters
; Notes:   
ClockKeyboardHandler:
	push	r16
	push	r17
	
	call	GetCurrentButton

	; Clock is responsible for switching the state to menu
	cpi		r16, BTN_SELECT
	brne	clock_kb_process_other

clock_kb_change_mode:		
	; Jump back to menu

	; Change the current state
	ldi		r16, MENU_SELECT
	sts		menu_state, r16

	; Set refresh timing back to menu
	ldi		r16, MENU_TIMING
	sts		time_delay, r16

	rjmp	clock_kb_ret_1

clock_kb_process_other:
	; Process any other key -> first check if clock is disabled.
	; We don't want use accidentally setting time when the clock is running
	lds		r17, clock_isEnabled
	tst		r17
	brne	clock_kb_ret_1

	; Click is disabled, process other keys
	call	ClockKeyHandler
	
clock_kb_disp:				; Used to display realtime time changes and to have an extra delay, as buttons are too fast.
	call	ClockDisplay

clock_kb_ret_1: ; Exit without delay
	pop		r17
	pop		r16
	ret

; Name:     clock_kb_process_key
; Purpose:  Processes the pressed key in r16.
; Entry:    r16 contains the key which was pressed
; Notes:	This function is there to shorten the clock_kb_handle, as it was way too long
ClockKeyHandler:
	push	r16
	push	r19

	push	ZH
	push	ZL

	cpi		r16, BTN_UP
	breq	clock_kb_up

	cpi		r16, BTN_DOWN
	breq	clock_kb_down

	cpi		r16, BTN_LEFT
	breq	clock_kb_left

	cpi		r16, BTN_RIGHT
	breq	clock_kb_right
	
	rjmp	clock_kb_process_end

clock_kb_up:				; Increment time by 1
	ldi		r19, 0 ; TODO: Change to R16
	call	ClockKeyboardAdjustTime
	rjmp	clock_kb_process_end

clock_kb_down:				; Decrement time by 1
	ldi		r19, 1
	call	ClockKeyboardAdjustTime
	rjmp	clock_kb_process_end

;		------ PROCESS RIGHT BUTTON ----------
clock_kb_right:				; Adjust which time element to increase/decrease to right
	lds		r16, kb_time_offset
	tst		r16
	breq	clock_kb_right_0

	dec		r16
	rjmp	clock_kb_right_1

clock_kb_right_0:
	ldi		r16, 4			; 4 == month

clock_kb_right_1:
	sts		kb_time_offset, r16
	rjmp	clock_kb_process_end


;		------ PROCESS LEFT BUTTON ----------
clock_kb_left:				; Adjust which time element to increase/decrease to left
	lds		r16, kb_time_offset
	cpi		r16, 4			; 4 == month
	breq	clock_kb_left_0

	inc		r16
	rjmp	clock_kb_left_1

clock_kb_left_0:
	clr		r16

clock_kb_left_1:
	sts		kb_time_offset, r16
	rjmp	clock_kb_process_end

clock_kb_process_end:
	pop		ZL
	pop		ZH

	pop		r19
	pop		r16
	ret

; ------------------------- JUMPTABLES ------------------------------------
; Jumptable for clock_kb_adjust_time (2 bytes)
clock_kb_adjust_time_get_jumptable:
	GetSeconds
	ret
	GetMinutes
	ret
	GetHours
	ret
	GetDay
	ret
	GetMonth
	ret
	GetYear
	ret

clock_kb_adjust_time_set_jumptable:
	jmp SetSeconds
	jmp SetMinutes
	jmp SetHours
	jmp SetDay
	jmp SetMonth
	jmp SetYear

; ------------------------- END OF JUMPTABLES ------------------------------------

; Name:     clock_kb_adjust_time
; Purpose:  Increments/Decrements time by an offset to a jumptable.
; Entry:	R19 to decide whether to increment(==0)/decrement(==1) the time.
; Notes:   
ClockKeyboardAdjustTime:
	push	r16
	push	r17
	push	r18

	push	ZH
	push	ZL

	lds		r17, kb_time_offset
	mov		r18, r17
	
	;		R17 = R17 * 3
	lsl		r17
	add		r17, r18

	ldi		ZH, high( clock_kb_adjust_time_get_jumptable )
	ldi		ZL, low( clock_kb_adjust_time_get_jumptable )

	clr		r18
	add		ZL, r17
	adc		ZH, r18

	icall

	tst		r19
	brne	clock_kb_adjust_time_dec

clock_kb_adjust_time_inc:
	inc		r16
	rjmp	clock_kb_adjust_time_set
clock_kb_adjust_time_dec:
	dec		r16

clock_kb_adjust_time_set:
	clr		r18
	lds		r17, kb_time_offset
	lsl		r17

	ldi		ZH, high( clock_kb_adjust_time_set_jumptable )
	ldi		ZL, low( clock_kb_adjust_time_set_jumptable )

	add		ZL, r17
	adc		ZH, r18

	icall

	pop		ZL
	pop		ZH

	pop		r18
	pop		r17
	pop		r16
	ret

#endif
