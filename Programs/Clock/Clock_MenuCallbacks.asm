/*
 * MenuCallbacks.asm
 *
 *  Created: 10.12.2017 23:28:21
 *   Author: Normunds
 */ 

#ifndef H_MENU_CALLBACKS
#define H_MENU_CALLBACKS

.include "..\Programs\Clock\Clock_Global.asm"
.include "..\Programs\Clock\Clock.asm"

.cseg

; TODO: Maybe menu should be entrusted with chaning the wait timings!!!
; As state switch can not only be clock/chrono


toggle_alarm_set:	.db "Alarm Set", 0
toggle_alarm_unset:	.db "Alarm Disabled", 0, 0

; Inverts the alarm_isEnabled flag and displays it.
callback_toggle_alarm:
	push	r16
	push	ZH
	push	ZL

	lds		r16, alarm_isEnabled
	tst		r16
	brne	callback_toggle_alarm_unset

	; Enable alarm
callback_toggle_alarm_set:
	ldi		r16, 1
	sts		alarm_isEnabled, r16

	ldi		ZH, high(toggle_alarm_set)
	ldi		ZL, low(toggle_alarm_set)

	rjmp	callback_toggle_alarm_end
	; Disable alarm
callback_toggle_alarm_unset:
	clr		r16
	sts		alarm_isEnabled, r16

	ldi		ZH, high(toggle_alarm_unset)
	ldi		ZL, low(toggle_alarm_unset)

callback_toggle_alarm_end:

	ldi		r16, lcd_Clear
	call	lcd_write_instruction_4d

	ldi		r16, lcd_LineOne
	call	lcd_write_string_4d_code

	; Show message for 1 second
	ldi		r16, 100
	call	delay_10x_ms

	pop		ZL
	pop		ZH
	pop		r16
	ret

; All this mode does is -> freeses the clock. (sets the isEnabled byte to false)
callback_set_clock:
	push	r16
	clr		r16

	; Set clock isEnabled byte to FALSE
	sts		clock_isEnabled, r16

	; Set mode back to clock
	ldi		r16, MENU_CLOCK
	sts		menu_state, r16

	; Change timings (500ms) for the blinking functionality.
	ldi		r16, CLOCK_SET_TIMING
	sts		time_delay, r16

	; Draw clock to display it faster instead of waiting for main loop
	call	ClockDisplay

	pop		r16
	ret

callback_set_alarm:
	push	r16

	call	AlarmStart

	; Mostly inherits everything from set_clock mode
	call	callback_set_clock

	; Set mode to alarm
	ldi		r16, MENU_ALARM
	sts		menu_state, r16

	pop		r16
	ret

; Responsible for switching back to clock mode
callback_switch_clock:
	push	r16
	
	; Change the current state
	ldi		r16, MENU_CLOCK
	sts		menu_state, r16

	; Set clock isEnabled byte to true
	ldi		r16, 1
	sts		clock_isEnabled, r16

	ldi		r16, CLOCK_TIMING
	sts		time_delay, r16

	; Load the correct callback for the starting mode (clock).
	;ldi		r16, CLOCK_TIMER_CALLBACK
	;sts		timer_callback_offset, r16

	; Make sure to restore the old ascii contents
	call	ConvertTimeAscii
	call	ConvertDateAscii

	; Draw clock to display it faster instead of waiting for main loop
	call	ClockDisplay

	pop		r16
	ret

callback_menu_minimise:	
	push	r16

	ldi		r16, 1
	sts		clock_minimise, r16

	pop		r16
	ret

callback_menu_exit:
	push	r16

	ldi		r16, 1
	sts		clock_exit, r16

	pop		r16
	ret

#endif


