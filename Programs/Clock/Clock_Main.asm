/*
 * Clock_Main.asm
 *
 *  Created: 28.12.2017 02:39:31
 *   Author: Normunds
 */ 

#ifndef H_CLOCK_MAIN
#define H_CLOCK_MAIN

.include "..\Programs\Clock\Clock_Global.asm"
.include "..\Programs\Clock\Clock_Interrupt.asm"
.include "..\Programs\Clock\Clock_Menu.asm"
.include "..\Programs\Clock\Clock.asm"
.include "..\Programs\Clock\Clock_Alarm.asm"

.cseg

; ----------------------------- JUMPTABLE FOR MAIN FUNCTIONS --------------------------
main_jumptable:
	jmp		main_select
	jmp		main_clock
	jmp		main_alarm


; Name:     main_clock
; Purpose:  Handles main clock
; Exit:     no parameters
; Notes: 
main_clock:
	call	ClockDisplay
	ret

; Name:     main_clock
; Purpose:  Handles main menu
; Exit:     no parameters
; Notes: 
main_select:
	call	MenuDisplay
	ret

; Name:     main_clock
; Purpose:  Handles main alarm
; Exit:     no parameters
; Notes:	Has the same functionality as clock, as it inherits from clock
main_alarm:
	call	main_clock
	ret

; Name:     main_call_callback
; Purpose:  Calls a callback in main_jumptable specified by offset in R16
; Exit:     no parameters
; Notes:	
main_call_callback:	
	push	r16
	push	r17

	push	ZH
	push	ZL

	clr		r17

	; R16 = R16 * 2
	lsl		r16

	ldi		ZH, high( main_jumptable )
	ldi		ZL, low( main_jumptable )

	clr		r17
	add		ZL, r16
	adc		ZH, r17

	icall

	pop		ZL
	pop		ZH

	pop		r17
	pop		r16
	ret

ClockMain:

	call	ClockGlobalInit
	call	MenuInit
	call	ClockInit
	call	AlarmInit

	; Register keyboard interrupt
	ldi		XH, high(MainClockKeyboardHandler)
	ldi		XL,  low(MainClockKeyboardHandler)
	call	RegisterKeyboardInt

	; Register timer interrupt
	ldi		XH, high(MainClockTimerHandler)
	ldi		XL,  low(MainClockTimerHandler)
	ldi		r25, 61
	call	RegisterTimerInt

	clock_loop:

		; Only draw when display is active
		call	IsMainProcess
		tst		r25
		breq	clock_loop

		; Call callback specified by r16
		lds		r16, menu_state
		call	main_call_callback
	
		lds		r16, clock_minimise
		tst		r16
		brne	clock_loop_minimise

		lds		r16, clock_exit
		tst		r16
		brne	clock_loop_exit

		rjmp	clock_loop_end

	clock_loop_minimise:
		; Reset the flag for next time
		clr		r16
		sts		clock_minimise, r16

		call	MinimiseProcess
		rjmp	clock_loop_end

	clock_loop_exit:
		call	ExitProcess
		
	clock_loop_end:
		lds		r16, time_delay
		call	delay_10x_ms

	jmp clock_loop



MainClockKeyboardHandler:
	call	HandleKeyboardInput
	ret

MainClockTimerHandler:
	push	r16

	lds		r16, clock_isEnabled
	tst		r16
	breq	MainClockTimerHandler_ret

	; Increment seconds
	GetSeconds
	inc		r16
	call	SetSeconds


MainClockTimerHandler_ret:
	pop		r16
	ret

#endif
