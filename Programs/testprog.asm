/*
 * testprog.asm
 *
 *  Created: 26.12.2017 00:30:49
 *   Author: Normunds
 */ 

.cseg

.include "LCD\LCD-lib.asm"
.include "User\Window.asm"
.include "User\ProcessUser.asm"
.include "User\Input.asm"
.include "OS_Functions\Process.asm"

testmain:

	ldi		XH, high(keyboard_test2)
	ldi		XL, low(keyboard_test2)

	call	RegisterKeyboardInt

	ldi		XH, high(timer_test2)
	ldi		XL, low(timer_test2)
	ldi		r25, 10
	call	RegisterTimerInt

	call	lcd_init_4d
	ldi		r17, 'a'
	ldi		r18, 0
loop2:
	; Only draw when display is active
	call	IsMainProcess
	tst		r25
	breq	loop2_cmp

	ldi		r16, lcd_Clear
	call	lcd_write_instruction_4d

	mov		r16, r17
	call	lcd_write_character_4d
	
loop2_cmp:
	ldi		r16, 10
	call	delay_10x_ms

	inc		r17
	cpi		r17, 'z'
	breq	testmain_end

	rjmp	loop2

testmain_end:
	ldi		r17, '0'
	;rjmp	loop2

	call	ExitProcess
	BREAK	; Should never reach this part

	
keyboard_test2:
	push	r16

	call	GetCurrentButton
	cpi		r16, BTN_SELECT
	brne	keyboard_test2_ret

	; Do a delay before switch to not have the next process
	; see our keyboard input

	call	MinimiseProcess

keyboard_test2_ret:
	pop		r16
	ret

timer_test2:
	push	r16
	lds		r16, overflow_count
	pop		r16
	ret